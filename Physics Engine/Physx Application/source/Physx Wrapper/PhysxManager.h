#pragma once

// STL includes
#include <vector>

// GLM includes
#include <glm\fwd.hpp>

// Physx includes
#include <PxPhysicsAPI.h>

namespace physxApp
{
	class PhysxFluid;
	class RagDoll;
	class PlayerController;
	class PhysxMemAlloc;
	class CollisionCallback;

	class PhysxManager
	{
		friend PhysxFluid;
		friend RagDoll;
		friend PlayerController;

	public:
		PhysxManager();
		~PhysxManager();

		// Initialises Physx and it's associated functionality
		void Initialise();

		// Ticks the Physx simulation
		void Update(float a_deltaTime);

		// Connects to the Physx Visual Debugger
		bool ConnectToPvd(const char* a_ip, int a_port, unsigned int a_timeout);

		// Creates a physx material, used to create rigid actors
		physx::PxMaterial* CreateMaterial(float a_staticFriction, float a_dynamicFriction, 
										  float a_restitution);

		// Creates a physx scene to store all rigid actors
		physx::PxScene* CreateScene(const glm::vec3& a_gravity, int a_cpuThreads);

		// Adds a dynamic box into the specified scene
		physx::PxRigidBody* AddDynamicBox(const physx::PxTransform& a_trans,
										  const physx::PxBoxGeometry& a_geo,
										  physx::PxMaterial* a_pMaterial,
										  physx::PxScene* a_pScene,
										  float a_density,
										  const char* a_name);

		// Adds a static box into the specified scene
		physx::PxRigidStatic* AddStaticBox(const physx::PxTransform& a_trans,
										   const physx::PxBoxGeometry& a_geo,
										   physx::PxMaterial* a_pMaterial,
										   physx::PxScene* a_pScene,
										   const char* a_name);

		// Adds a dynamic sphere into the specified scene
		physx::PxRigidBody* AddDynamicSphere(const physx::PxTransform& a_trans,
											 const physx::PxSphereGeometry& a_geo,
											 physx::PxMaterial* a_pMaterial,
											 physx::PxScene* a_pScene,
											 float a_density,
											 const char* a_name);

		// Adds a static box into the specified scene
		physx::PxRigidStatic* AddStaticSphere(const physx::PxTransform& a_trans,
											  const physx::PxSphereGeometry& a_geo,
											  physx::PxMaterial* a_pMaterial,
											  physx::PxScene* a_pScene,
											  const char* a_name);

		// Adds a static plane into the specified scene
		physx::PxRigidStatic* AddStaticPlane(const physx::PxTransform& a_trans,
											 const physx::PxPlaneGeometry& a_geo,
											 physx::PxMaterial* a_pMaterial,
											 physx::PxScene* a_pScene,
											 const char* a_name);

		physx::PxRigidBody* AddDynamicCapsule(const physx::PxTransform& a_trans,
											  const physx::PxCapsuleGeometry& a_geo, 
											  physx::PxMaterial* a_pMaterial,
											  physx::PxScene* a_pScene,
											  float a_density,
											  const char* a_name);

		physx::PxRigidStatic* AddStaticCapsule(const physx::PxTransform& a_trans,
											  const physx::PxCapsuleGeometry& a_geo,
											  physx::PxMaterial* a_pMaterial,
											  physx::PxScene* a_pScene,
											  const char* a_name);

		physx::PxRigidActor* CreateTriggerBox(const physx::PxTransform& a_trans,
											  const physx::PxBoxGeometry& a_geo,
											  physx::PxMaterial* a_pMaterial,
											  physx::PxScene* a_pScene,
											  const char* a_name);

		// Returns all dynamic actors in all scenes
		inline const std::vector<physx::PxRigidBody*>& GetDynamicActors() const
		{
			return m_dynamicActors;
		}

		// Returns all static actors in all scenes
		inline const std::vector<physx::PxRigidStatic*>& GetStaticActors() const
		{
			return m_staticActors;
		}

		// Returns all scenes currently created
		inline const std::vector<physx::PxScene*>& GetScenes() const
		{
			return m_scenes;
		}

		inline const std::vector<physx::PxRigidActor*>& GetTriggers() const
		{
			return m_triggers;
		}

	private:
		physx::PxFoundation* m_pPhysicsFoundation;					// Used to initialise the Physx SDK
		physx::PxPhysics* m_pPhysics;								// Physx singleton and factory
		physx::PxDefaultErrorCallback m_DefaultErrorCallback;		// Error callback for Physx errors
		physx::PxDefaultAllocator m_DefaultAllocatorCallback;		// Default allocator callback for Physx
		physx::PxSimulationFilterShader m_DefaultFilterShader;		// Default shader used by Physx						
		physx::PxCooking* m_pPhysicsCooker;							// Physics cooker, used to create mesh based colliders
		physx::PxVisualDebuggerConnection* m_pPVDconnection;

		PhysxMemAlloc* m_pPhysxMemAlloc;							// Physx memory allocator

		CollisionCallback* m_pCollisionCallback;					// Physx collision callback

		std::vector<physx::PxRigidBody*> m_dynamicActors;			// List of all dynamic actors within all scenes
		std::vector<physx::PxRigidStatic*> m_staticActors;			// List of all static actors within all scenes
		std::vector<physx::PxRigidActor*> m_triggers;				// List of all triggers

		std::vector<physx::PxScene*> m_scenes;						// List of all scenes to update

		std::vector<physx::PxMaterial*> m_materials;				// List of all materials used in all scenes

		inline physx::PxPhysics* GetPhysics() const
		{
			return m_pPhysics;
		}
	};
}
