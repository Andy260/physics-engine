#include "PhysxManager.h"

// STL includes
#include <iostream>

// Physx includes
#include "PhysxMemAlloc.h"
#include <PxPhysicsAPI.h>
#include <PxScene.h>
#include <pvd/PxVisualDebugger.h>

// GLM includes
#include <glm\vec3.hpp>
#include <glm\gtx\quaternion.hpp>

// Project includes
#include <Trigger Volumes\FilterShader.h>
#include <Trigger Volumes\CollisionCallback.h>

using namespace physxApp;

PhysxManager::PhysxManager()
{
}

PhysxManager::~PhysxManager()
{
	// Release all dynamic actors
	for (auto &dyamaicActor : m_dynamicActors)
	{
		dyamaicActor->release();
		dyamaicActor = nullptr;
	}

	// Release all static actors
	for (auto &staticActor : m_staticActors)
	{
		staticActor->release();
		staticActor = nullptr;
	}

	// Relase all scenes
	for (auto &scene : m_scenes)
	{
		scene->release();
		scene = nullptr;
	}

	// Disconnect PVD
	if (m_pPVDconnection != nullptr)
	{
		m_pPVDconnection->disconnect();
		m_pPVDconnection->release();
		m_pPVDconnection = nullptr;
	}

	// Release Physx
	if (m_pPhysics != nullptr)
	{
		m_pPhysics->release();
		m_pPhysics = nullptr;
	}

	// Release Physx foundation
	if (m_pPhysicsFoundation != nullptr)
	{
		m_pPhysicsFoundation->release();
		m_pPhysicsFoundation = nullptr;
	}

	// Delete Physx memory allocator
	if (m_pPhysxMemAlloc != nullptr)
	{
		delete m_pPhysxMemAlloc;
	}

	delete m_pCollisionCallback;
}

void PhysxManager::Initialise()
{
	// Initialise Physx
	m_pPhysxMemAlloc = new PhysxMemAlloc();

	m_pPhysicsFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, *m_pPhysxMemAlloc,
		m_DefaultErrorCallback);

	m_pPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_pPhysicsFoundation,
		physx::PxTolerancesScale());

	PxInitExtensions(*m_pPhysics);

	m_pCollisionCallback = new CollisionCallback();
}

void PhysxManager::Update(float a_deltaTime)
{
	// Don't tick if not enough time has passed
	if (a_deltaTime <= 0)
	{
		return;
	}

	// Update all scenes
	for (auto &scene : m_scenes)
	{
		scene->simulate(a_deltaTime);

		while (!scene->fetchResults())
		{
			// Don't need to do anything here yet, but we have to fetch results
		}
	}
}

bool PhysxManager::ConnectToPvd(const char* a_ip, int a_port, unsigned int a_timeout)
{
	// Check if PvdConnection manager is avaliable on this platform
	if (m_pPhysics->getPvdConnectionManager() == nullptr)
	{
		std::cerr << "PvD unavaliable on this platform" << std::endl;
		return false;
	}

	physx::PxVisualDebuggerConnectionFlags connectionFlags =
		physx::PxVisualDebuggerExt::getAllConnectionFlags();

	// And now try to connect PxVisualDebuggerExt
	m_pPVDconnection =
		physx::PxVisualDebuggerExt::createConnection(
		m_pPhysics->getPvdConnectionManager(),
		a_ip, a_port, a_timeout, connectionFlags);

	return true;
}

physx::PxMaterial* PhysxManager::CreateMaterial(float a_staticFriction, 
	float a_dynamicFriction, float a_restitution)
{
	return m_pPhysics->createMaterial(a_staticFriction, a_dynamicFriction, a_restitution);
}

physx::PxScene* PhysxManager::CreateScene(const glm::vec3& a_gravity, 
	int a_cpuThreads)
{
	// Create scene descriptor
	physx::PxSceneDesc sceneDesc(m_pPhysics->getTolerancesScale());
	sceneDesc.gravity					= physx::PxVec3(a_gravity.x, a_gravity.y, a_gravity.z);
	sceneDesc.filterShader				= &physx::PxDefaultSimulationFilterShader;
	sceneDesc.cpuDispatcher				= physx::PxDefaultCpuDispatcherCreate(a_cpuThreads);
	sceneDesc.filterShader				= &FilterShader::PhysxFilterShader;

	// Create scene and add it to the list
	physx::PxScene* pScene = m_pPhysics->createScene(sceneDesc);
	m_scenes.push_back(pScene);

	pScene->setSimulationEventCallback(m_pCollisionCallback);

	return pScene;
}

// Adds a dynamic box into the specified scene
physx::PxRigidBody* PhysxManager::AddDynamicBox(const physx::PxTransform& a_trans,
	const physx::PxBoxGeometry& a_geo, physx::PxMaterial* a_pMaterial, 
	physx::PxScene* a_pScene, float a_density, const char* a_name)
{
	physx::PxRigidBody* pRigidBody = physx::PxCreateDynamic(*m_pPhysics, a_trans, a_geo, 
		*a_pMaterial, a_density);

	// Add actor to scene and main actor list
	a_pScene->addActor(*pRigidBody);
	m_dynamicActors.push_back(pRigidBody);

	pRigidBody->setName(a_name);

	return pRigidBody;
}

// Adds a static box into the specified scene
physx::PxRigidStatic* PhysxManager::AddStaticBox(const physx::PxTransform& a_trans,
	const physx::PxBoxGeometry& a_geo, physx::PxMaterial* a_pMaterial, 
	physx::PxScene* a_pScene, const char* a_name)
{
	physx::PxRigidStatic* pRigidBody = physx::PxCreateStatic(*m_pPhysics, a_trans,
		a_geo, *a_pMaterial);

	// Add actor to scene and main actor list
	a_pScene->addActor(*pRigidBody);
	m_staticActors.push_back(pRigidBody);

	pRigidBody->setName(a_name);

	return pRigidBody;
}

// Adds a dynamic sphere into the specified scene
physx::PxRigidBody* PhysxManager::AddDynamicSphere(const physx::PxTransform& a_trans,
	const physx::PxSphereGeometry& a_geo, physx::PxMaterial* a_pMaterial, 
	physx::PxScene* a_pScene, float a_density, const char* a_name)
{
	physx::PxRigidBody* pRigidBody = physx::PxCreateDynamic(*m_pPhysics, a_trans, a_geo,
		*a_pMaterial, a_density);

	// Add actor to scene and main actor list
	a_pScene->addActor(*pRigidBody);
	m_dynamicActors.push_back(pRigidBody);

	pRigidBody->setName(a_name);

	return pRigidBody;
}

// Adds a static box into the specified scene
physx::PxRigidStatic* PhysxManager::AddStaticSphere(const physx::PxTransform& a_trans,
	const physx::PxSphereGeometry& a_geo, physx::PxMaterial* a_pMaterial, 
	physx::PxScene* a_pScene, const char* a_name)
{
	physx::PxRigidStatic* pRigidBody = physx::PxCreateStatic(*m_pPhysics, a_trans,
		a_geo, *a_pMaterial);

	// Add actor to scene and main actor list
	a_pScene->addActor(*pRigidBody);
	m_staticActors.push_back(pRigidBody);

	pRigidBody->setName(a_name);

	return pRigidBody;
}

// Adds a static plane into the specified scene
physx::PxRigidStatic* PhysxManager::AddStaticPlane(const physx::PxTransform& a_trans,
	const physx::PxPlaneGeometry& a_geo, physx::PxMaterial* a_pMaterial, 
	physx::PxScene* a_pScene, const char* a_name)
{
	physx::PxRigidStatic* pRigidBody = physx::PxCreateStatic(*m_pPhysics, a_trans,
		physx::PxPlaneGeometry(), *a_pMaterial);

	// Add actor to scene and main actor list
	a_pScene->addActor(*pRigidBody);
	m_staticActors.push_back(pRigidBody);

	pRigidBody->setName(a_name);

	return pRigidBody;
}

physx::PxRigidBody* PhysxManager::AddDynamicCapsule(const physx::PxTransform& a_trans,
	const physx::PxCapsuleGeometry& a_geo, physx::PxMaterial* a_pMaterial, 
	physx::PxScene* a_pScene, float a_density, const char* a_name)
{
	physx::PxRigidBody* pRigidBody = physx::PxCreateDynamic(*m_pPhysics, a_trans, a_geo,
		*a_pMaterial, a_density);

	// Add actor to scene and main actor list
	a_pScene->addActor(*pRigidBody);
	m_dynamicActors.push_back(pRigidBody);

	pRigidBody->setName(a_name);

	return pRigidBody;
}

physx::PxRigidStatic* PhysxManager::AddStaticCapsule(const physx::PxTransform& a_trans,
	const physx::PxCapsuleGeometry& a_geo, physx::PxMaterial* a_pMaterial,
	physx::PxScene* a_pScene, const char* a_name)
{
	physx::PxRigidStatic* pRigidBody = physx::PxCreateStatic(*m_pPhysics, a_trans,
		physx::PxPlaneGeometry(), *a_pMaterial);

	// Add actor to scene and main actor list
	a_pScene->addActor(*pRigidBody);
	m_staticActors.push_back(pRigidBody);

	pRigidBody->setName(a_name);

	return pRigidBody;
}

physx::PxRigidActor* PhysxManager::CreateTriggerBox(const physx::PxTransform& a_trans,
	const physx::PxBoxGeometry& a_geo, physx::PxMaterial* a_pMaterial, 
	physx::PxScene* a_pScene, const char* a_name)
{
	physx::PxRigidStatic* pRigidBody = physx::PxCreateStatic(*m_pPhysics, a_trans,
		a_geo, *a_pMaterial);

	// Add actor to scene and main actor list
	a_pScene->addActor(*pRigidBody);
	m_triggers.push_back(pRigidBody);

	pRigidBody->setName(a_name);

	FilterShader::SetActorAsTrigger(pRigidBody);

	return pRigidBody;
}
