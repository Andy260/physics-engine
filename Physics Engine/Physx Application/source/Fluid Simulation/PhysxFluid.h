#pragma once

namespace physx
{
	class PxParticleFluid;
	class PxScene;
	class PxVec3;
}

class ParticleFluidEmitter;

namespace physxApp
{
	class PhysxManager;

	class PhysxFluid
	{
		friend PhysxManager;

	public:
		PhysxFluid(PhysxManager* a_pPhysxManager);
		~PhysxFluid();

		void Initialise(const physx::PxVec3& a_spawnPos,
			physx::PxScene* a_scene);

		void Update(float a_deltaTime);

		void Draw();

	private:
		PhysxManager* m_pPhysxManager;

		physx::PxScene* m_pScene;

		physx::PxParticleFluid* m_pParticleFluid;

		ParticleFluidEmitter* m_pParticleEmitter;
	};
}
