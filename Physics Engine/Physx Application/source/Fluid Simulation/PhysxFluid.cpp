#include "PhysxFluid.h"

// Project includes
#include <Physx Wrapper\PhysxManager.h>
#include <Fluid Simulation\Particle Emitter\ParticleFluidEmitter.h>

// Physx includes
#include <PxPhysicsAPI.h>
#include <particles\PxParticleSystem.h>

using namespace physxApp;

PhysxFluid::PhysxFluid(PhysxManager* a_pPhysxManager)
	: m_pPhysxManager(a_pPhysxManager)
{
}

PhysxFluid::~PhysxFluid()
{
	// TODO: Clean up Physx
}

void PhysxFluid::Initialise(const physx::PxVec3& a_spawnPos, 
	physx::PxScene* a_scene)
{
	// Move variable into temp physx type, to ensure correct casting
	physx::PxU32 maxParticles(5000);

	// Get Physx singleton
	physx::PxPhysics* pPhysics = m_pPhysxManager->GetPhysics();

	// Create particle fluid system
	m_pParticleFluid = pPhysics->createParticleFluid(maxParticles, false);
	
	// Set initial desired particle settings
	m_pParticleFluid->setRestParticleDistance(0.3f);
	m_pParticleFluid->setDynamicFriction(0.1f);
	m_pParticleFluid->setStaticFriction(0.1f);
	m_pParticleFluid->setDamping(0.1f);
	m_pParticleFluid->setParticleMass(0.1f);
	m_pParticleFluid->setRestitution(0);
	m_pParticleFluid->setParticleBaseFlag(physx::PxParticleBaseFlag::eCOLLISION_TWOWAY, true);
	m_pParticleFluid->setStiffness(100);

	if (m_pParticleFluid)
	{
		a_scene->addActor(*m_pParticleFluid);
		m_pParticleEmitter = new ParticleFluidEmitter(maxParticles, 
			a_spawnPos, m_pParticleFluid, 0.01f);
		m_pParticleEmitter->setStartVelocityRange(-0.001f, -250.0f, -0.001f, 0.001f, -250.0f, 
			0.001f);
	}
}

void PhysxFluid::Update(float a_deltaTime)
{
	if (m_pParticleEmitter)
	{
		m_pParticleEmitter->update(a_deltaTime);
	}
}

void PhysxFluid::Draw()
{
	if (m_pParticleEmitter)
	{
		m_pParticleEmitter->renderParticles();
	}
}
