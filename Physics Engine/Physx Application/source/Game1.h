#pragma once
#include <Game.h>

// STL includes
#include <vector>

// Project includes
#include <Physx Wrapper\PhysxManager.h>

// OpenGL Engine includes
#include <Camera\FlyCamera.h>

// Project includes
#include <Graphics\PhysxVisualiser.h>
#include <Fluid Simulation\PhysxFluid.h>
#include <Ragdoll\RagDoll.h>
#include <Player Controller\PlayerController.h>

namespace physx { class PxRigidStatic; }

namespace physxApp
{
	class Game1 : public Game
	{
	public:
		Game1();
		~Game1();

		// Used to initialise the application
		void Initialise()						override final;

		// Called every frame to update the game state
		void Update(const float &a_deltaTime)	override final;

		// Renders the current game state
		void Draw(const float &a_deltaTime)		override final;

		// Cleans up the application before application closure
		void Shutdown()							override final;

	private:
		physx::PxScene* m_PhysicsScene;								// Main physics scene
		physx::PxMaterial* m_PhysicsMaterial;						// Material used by all rigid bodies within the main physics scene							

		FlyCamera m_camera;											// Fly camera used to render the scene

		PhysxManager m_physxManager;								// Physx manager, for managing physx at a high level

		PhysxVisualiser m_physxVisualiser;							// Used to render all rigid bodies in all scenes

		PhysxFluid m_physxFluid;

		RagDoll m_ragDoll;

		PlayerController m_playerController;

		bool m_simulateFluid;

		void SetupPhysx();

		void SetupFluid(physx::PxRigidStatic* a_pGround);

		void SetupRagDoll();

		void SetupPlayerController();

		void SetupBasicScene();

		void SetupTriggerVolume();
	};
}
