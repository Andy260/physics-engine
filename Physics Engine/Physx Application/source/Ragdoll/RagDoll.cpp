#include "RagDoll.h"

// Physx includes
#include <PxPhysicsAPI.h>

// Project includes
#include <Physx Wrapper\PhysxManager.h>

using namespace physxApp;

RagDoll::RagDoll(PhysxManager* a_pPhysxManager)
	: m_pPhysxManager(a_pPhysxManager)
{
	
}

RagDoll::~RagDoll()
{
	for (unsigned int i = 0; i < Bone::BONE_COUNT; ++i)
	{
		physx::PxArticulationLink* pNode = m_nodesArray[i].linkPtr;

		if (pNode != nullptr)
		{
			pNode->release();
		}
	}

	delete[] m_nodesArray;
}

void RagDoll::Initialise(physx::PxTransform& a_worldTrans, float a_scaleFactor, 
	physx::PxMaterial* a_pMaterial, physx::PxScene* a_pScene)
{
	// Get Physx singleton
	physx::PxPhysics* pPhysics = m_pPhysxManager->GetPhysics();

	// Create the articulation for our ragdoll
	physx::PxArticulation* pArticulation = pPhysics->createArticulation();
	
	// Create the link in the articulation

	CreateNodes();


	//physx::PxArticulationLink* pLink = pArticulation->createLink(nullptr, physx::PxTransform(0, 0, 0));

	// Process nodes
	for (int node = 0; node < BONE_COUNT; ++node)
	{
		// Get pointer to current node
		RagDollNode* pCurrentNode	= &m_nodesArray[node];

		RagDollNode* pParentNode	= nullptr;

		// Get scaled values for capsule
		float radius = pCurrentNode->radius * a_scaleFactor;
		float halfLength = pCurrentNode->halfLength * a_scaleFactor;
		float childHalfLength = radius + halfLength;
		float parentHalfLength = 0.0f;	// Will be set later if there is a parent

		// Get a pointer to the parent
		physx::PxArticulationLink* pParentLink = nullptr;
		pCurrentNode->scaledGlobalPos = a_worldTrans.p;

		if (pCurrentNode->parentNodeIdx != Bone::NO_PARENT)
		{
			// There is a parent then we need to work out our local position for the link
			// Get a pointer to the parent node
			pParentNode = (m_nodesArray + pCurrentNode->parentNodeIdx);

			// Get a pointer to the link for the parent
			pParentLink = pParentNode->linkPtr;

			parentHalfLength = (pParentNode->radius + pParentNode->halfLength) * a_scaleFactor;

			// Work out the local position of the node
			physx::PxVec3 currentRelative = pCurrentNode->childLinkPos * 
				pCurrentNode->globalRotation.rotate(physx::PxVec3(childHalfLength, 0, 0));

			physx::PxVec3 parentRelative = -pCurrentNode->parentLinkPos * 
				pParentNode->globalRotation.rotate(physx::PxVec3(parentHalfLength, 0, 0));

			pCurrentNode->scaledGlobalPos = pParentNode->scaledGlobalPos - 
				(parentRelative + currentRelative);
		}

		// Build the transform for the link
		physx::PxTransform linkTransform = physx::PxTransform(pCurrentNode->scaledGlobalPos, 
			pCurrentNode->globalRotation);

		//linkTransform = physx::PxTransform(0,0,0);

		// Create the link in the articulation
		physx::PxArticulationLink* pLink = pArticulation->createLink(pParentLink, 
			linkTransform);

		// Assign name to actor
		pLink->setName(pCurrentNode->name);

		// Add the pointer to this link into the ragdoll data so we have it for 
		// later when we want to link to it
		pCurrentNode->linkPtr = pLink;
		

		float jointSpace = 0.1f; // Gap between joints

		float capsuleHalfLength = 
			(halfLength > jointSpace ? halfLength - jointSpace : 0) + 0.01f;

		physx::PxCapsuleGeometry capsule(radius, capsuleHalfLength);

		// Adds a capsule collider to the link
		pLink->createShape(capsule, *a_pMaterial);

		// Adds some mass (should really be part of data)
		physx::PxRigidBodyExt::updateMassAndInertia(*pLink, 50.0f);

		if (pCurrentNode->parentNodeIdx != NO_PARENT)
		{
			// Get the pointer to the joint from the link
			physx::PxArticulationJoint* pJoint = pLink->getInboundJoint();

			// Get the relative rotation of this link
			physx::PxQuat frameRotation = pParentNode->globalRotation.getConjugate() 
				* pCurrentNode->globalRotation;

			// Set the parent contraint frame
			physx::PxTransform parentConstraintFrame(
				physx::PxVec3(pCurrentNode->parentLinkPos * parentHalfLength, 0, 0), 
					frameRotation);

			// Set the child constraint frame (this the constraint frame of 
			// the newly added link)
			physx::PxTransform thisConstraintFrame(
				physx::PxVec3(pCurrentNode->childLinkPos * childHalfLength, 0, 0));

			// Setup the poses for the joint so it is in the correct place
			pJoint->setParentPose(parentConstraintFrame);
			pJoint->setChildPose(thisConstraintFrame);

			// Set up some constraints to stop it flopping around
			pJoint->setStiffness(20.0f);
			pJoint->setDamping(20.0f);
			pJoint->setSwingLimit(0.4f, 0.4f);
			pJoint->setSwingLimitEnabled(true);
			pJoint->setTwistLimit(-0.1f, 0.1f);
			pJoint->setTwistLimitEnabled(true);
		}
	}

	// Add rag doll to the scene
	a_pScene->addArticulation(*pArticulation);

	// Add all created rigidbodies to Physx Manager for rendering
	for (unsigned int i = 0; i < Bone::BONE_COUNT; ++i)
	{
		physx::PxRigidBody* pRigidBody = m_nodesArray[i].linkPtr;

		m_pPhysxManager->m_dynamicActors.push_back(pRigidBody);
	}
}

void RagDoll::CreateNodes()
{
	m_nodesArray = new RagDollNode[16]
	{
		RagDollNode(physx::PxQuat(physx::PxPi / 2.0f, Z_AXIS), NO_PARENT, 
			1.0f, 3.0f, 1.0f, 1.0f, "lower spine"),

		RagDollNode(physx::PxQuat(physx::PxPi, Z_AXIS), LOWER_SPINE, 
			1.0f, 1.0f, -1.0f, 1.0f, "left pelvis"),

		RagDollNode(physx::PxQuat(0.0f, Z_AXIS), LOWER_SPINE, 1.0f, 
			1.0f, -1.0f, 1.0f, "right pelvis"),

		RagDollNode(physx::PxQuat(physx::PxPi / 2.0f + 0.2f, Z_AXIS), 
			LEFT_PELVIS, 5.0f, 2.0f, -1.0f, 1.0f, "L upper leg"),

		RagDollNode(physx::PxQuat(physx::PxPi / 2.0f - 0.2f, Z_AXIS), 
			RIGHT_PELVIS, 5.0f, 2.0f, -1.0f, 1.0f, "R upper leg"),

		RagDollNode(physx::PxQuat(physx::PxPi / 2.0f + 0.2f, Z_AXIS), 
			LEFT_UPPER_LEG, 5.0f, 1.75f, -1.0f, 1.0f, "L lower leg"),

		RagDollNode(physx::PxQuat(physx::PxPi / 2.0f - 0.2f, Z_AXIS), 
			RIGHT_UPPER_LEG, 5.0f, 1.75f, -1.0f, 1.0f, "R lowerleg"),

		RagDollNode(physx::PxQuat(physx::PxPi / 2.0f, Z_AXIS), 
			LOWER_SPINE, 1.0f, 3.0f, 1.0f, -1.0f, "upper spine"),

		RagDollNode(physx::PxQuat(physx::PxPi, Z_AXIS), UPPER_SPINE, 
			1.0f, 1.5f, 1.0f, 1.0f, "left clavicle"),

		RagDollNode(physx::PxQuat(0.0f, Z_AXIS), UPPER_SPINE, 1.0f, 
			1.5f, 1.0f, 1.0f, "right clavicle"),

		RagDollNode(physx::PxQuat(physx::PxPi / 2.0f, Z_AXIS), 
			UPPER_SPINE, 1.0f, 1.0f, 1.0f, -1.0f, "neck"),

		RagDollNode(physx::PxQuat(physx::PxPi / 2.0f, Z_AXIS), 
			NECK, 1.0f, 3.0f, 1.0f, -1.0f, "HEAD"),

		RagDollNode(physx::PxQuat(physx::PxPi - 0.3f, Z_AXIS), 
			LEFT_CLAVICLE, 3.0f, 1.5f, -1.0f, 1.0f, "left upper arm"),

		RagDollNode(physx::PxQuat(0.3f, Z_AXIS), RIGHT_CLAVICLE, 3.0f, 1.5f, 
			-1.0f, 1.0f, "right upper arm"),

		RagDollNode(physx::PxQuat(physx::PxPi - 0.3f, Z_AXIS), 
			LEFT_UPPER_ARM, 3.0f, 1.0f, -1.0f, 1.0f, "left lower arm"),

		RagDollNode(physx::PxQuat(0.3f, Z_AXIS), RIGHT_UPPER_ARM, 
			3.0f, 1.0f, -1.0f, 1.0f, "right lower arm"),
	};
}

void RagDoll::ApplyImpulse(const physx::PxVec3& a_impulse, Bone a_bone)
{
	physx::PxRigidBody* a_RigidBody = m_nodesArray[a_bone].linkPtr;

	a_RigidBody->addForce(a_impulse, physx::PxForceMode::eIMPULSE);
}

void RagDoll::ApplyImpulse(const physx::PxVec3& a_impulse)
{
	for (unsigned int i = 0; i < Bone::BONE_COUNT; ++i)
	{
		physx::PxRigidBody* a_RigidBody = m_nodesArray[i].linkPtr;

		a_RigidBody->addForce(a_impulse, physx::PxForceMode::eIMPULSE);
	}
}
