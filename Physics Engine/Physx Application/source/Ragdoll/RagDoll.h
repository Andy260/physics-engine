#pragma once

// STL includes
#include <vector>

// Physx includes
#include <foundation\PxVec3.h>
#include <foundation\PxQuat.h>
#include <foundation\PxTransform.h>

namespace physx { class PxMaterial; class PxArticulationLink; class PxScene; 
	class PxArticulation; }

namespace physxApp
{
	class PhysxManager;

	class RagDoll
	{
	public:
		enum Bone
		{
			NO_PARENT = -1,
			LOWER_SPINE = 0,
			LEFT_PELVIS = 1,
			RIGHT_PELVIS = 2,
			LEFT_UPPER_LEG = 3,
			RIGHT_UPPER_LEG = 4,
			LEFT_LOWER_LEG = 5,
			RIGHT_LOWER_LEG = 6,
			UPPER_SPINE = 7,
			LEFT_CLAVICLE = 8,
			RIGHT_CLAVICLE = 9,
			NECK = 10,
			HEAD = 11,
			LEFT_UPPER_ARM = 12,
			RIGHT_UPPER_ARM = 13,
			LEFT_LOWER_ARM = 14,
			RIGHT_LOWER_ARM = 15,
			BONE_COUNT = RIGHT_LOWER_ARM + 1
		};

		RagDoll(PhysxManager* a_pPhysxManager);
		~RagDoll();

		void Initialise(physx::PxTransform& a_worldTrans, float a_scaleFactor, 
						physx::PxMaterial* a_pMaterial, physx::PxScene* a_pScene);

		// Applies an impulse to a specific bone in the ragdoll
		void ApplyImpulse(const physx::PxVec3& a_impulse, Bone a_bone);
		// Applies an impulse to the entire ragdoll
		void ApplyImpulse(const physx::PxVec3& a_impulse);

	private:
		struct RagDollNode
		{
			physx::PxQuat globalRotation;		// Rotation of link within model space
			physx::PxVec3 scaledGlobalPos;		// Position of this link centre in world space
			Bone parentNodeIdx;					// Index of the parent node
			float halfLength;					// Half length of the capsule of this node
			float radius;						// Radius of capsule for this node
			float parentLinkPos;				// Relative position of link centre point in parent to this node
			float childLinkPos;					// Relative position of link centre in child
			char* name;							// Name of the link
			physx::PxArticulationLink* linkPtr;	// Pointer to parent link

			RagDollNode(const physx::PxQuat& a_globalRotation, Bone a_parentNodeIdx,
				float a_halfLength, float a_radius, float a_parentLinkPos, 
				float a_childLinkPos, char* a_name)	: 
				globalRotation(a_globalRotation), 
				parentNodeIdx(a_parentNodeIdx),
				halfLength(a_halfLength), 
				radius(a_radius), 
				parentLinkPos(a_parentLinkPos),
				childLinkPos(a_childLinkPos), 
				name(a_name),
				linkPtr(nullptr)
			{}
		};

		const physx::PxVec3 X_AXIS = physx::PxVec3(1.0f, 0.0f, 0.0f);
		const physx::PxVec3 Y_AXIS = physx::PxVec3(0.0f, 1.0f, 0.0f);
		const physx::PxVec3 Z_AXIS = physx::PxVec3(0.0f, 0.0f, 1.0f);

		PhysxManager* m_pPhysxManager;

		RagDollNode* m_nodesArray;	// Collection of all bones and their associated nodes

		physx::PxArticulation* m_pArticulation;

		void CreateNodes();
	};
}
