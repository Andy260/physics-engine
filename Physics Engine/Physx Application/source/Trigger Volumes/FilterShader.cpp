#include "FilterShader.h"

// Physx includes
#include <PxRigidActor.h>
#include <PxRigidStatic.h>

// STL includes
#include <assert.h>

using namespace physxApp;

physx::PxFilterFlags FilterShader::PhysxFilterShader(physx::PxFilterObjectAttributes a_attributes0,
	physx::PxFilterData a_filterData0, physx::PxFilterObjectAttributes a_attributes1,
	physx::PxFilterData a_filterData1, physx::PxPairFlags& a_pairFlags,
	const void* a_pConstantBlock, physx::PxU32 a_constantBlockSize)
{
	// Let triggers through
	if (physx::PxFilterObjectIsTrigger(a_attributes0) ||
		physx::PxFilterObjectIsTrigger(a_attributes1))
	{
		a_pairFlags = physx::PxPairFlag::eTRIGGER_DEFAULT;

		return physx::PxFilterFlag::eDEFAULT;
	}

	// Generate contacts for all that were not filtered above
	a_pairFlags = physx::PxPairFlag::eCONTACT_DEFAULT;

	// Trigger the contact callback for pairs (A,B) where
	// the filtermask of A contains the ID of B and vice versa.
	if ((a_filterData0.word0 & a_filterData1.word1) && (a_filterData1.word0 &
		a_filterData0.word1))
	{
		a_pairFlags |= physx::PxPairFlag::eNOTIFY_TOUCH_FOUND |
			physx::PxPairFlag::eNOTIFY_TOUCH_LOST;
	}

	return physx::PxFilterFlag::eDEFAULT;
}

void FilterShader::SetActorFilter(physx::PxRigidActor* a_pActor, 
	physx::PxU32 a_filterGroup, physx::PxU32 a_filterMask)
{
	physx::PxFilterData filterData;

	// word0 = own ID
	filterData.word0 = a_filterGroup;
	// word1 = ID mask to filter pairs that trigger a contact callback
	filterData.word1 = a_filterMask;

	const physx::PxU32 numShapes = a_pActor->getNbShapes();
	physx::PxShape** shapes = (physx::PxShape**)_aligned_malloc(
		sizeof(physx::PxShape*)*numShapes, 16);
	
	a_pActor->getShapes(shapes, numShapes);
	
	for (physx::PxU32 i = 0; i < numShapes; ++i)
	{
		physx::PxShape* shape = shapes[i];
		shape->setSimulationFilterData(filterData);
	}

	_aligned_free(shapes);
}

void FilterShader::SetActorAsTrigger(physx::PxRigidActor* a_pActor)
{
	physx::PxRigidStatic* pStaticActor = a_pActor->is<physx::PxRigidStatic>();
	assert(pStaticActor);

	const physx::PxU32 numShapes = pStaticActor->getNbShapes();
	
	physx::PxShape** shapes = (physx::PxShape**)_aligned_malloc(
		sizeof(physx::PxShape*)*numShapes, 16);
	
	pStaticActor->getShapes(shapes, numShapes);

	for (physx::PxU32 i = 0; i < numShapes; i++)
	{
		shapes[i]->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
		shapes[i]->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
	}
}
