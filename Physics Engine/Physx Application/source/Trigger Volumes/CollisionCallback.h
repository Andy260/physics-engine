#pragma once
#include <PxSimulationEventCallback.h>

namespace physxApp
{
	class CollisionCallback : public physx::PxSimulationEventCallback
	{
	public:
		CollisionCallback() {}
		~CollisionCallback() {}

		// This is called when a breakable constraint breaks.
		virtual void onConstraintBreak(physx::PxConstraintInfo* a_constraints, 
									   physx::PxU32 a_count)							override final;

		// This is called during PxScene::fetchResults with the actors 
		// which have just been woken up.
		virtual void onWake(physx::PxActor** a_actors, physx::PxU32 a_count)			override final;

		// This is called during PxScene::fetchResults with the actors which 
		// have just been put to sleep.
		virtual void onSleep(physx::PxActor** a_actors, physx::PxU32 a_count)			override final;

		// The user needs to implement this interface class in order to be 
		// notified when certain contact events occur.
		virtual void onContact(const physx::PxContactPairHeader& a_pairHeader, 
					   const physx::PxContactPair* a_pairs, 
					   physx::PxU32 a_nbPairs)											override final;

		// This is called during PxScene::fetchResults with the current 
		// trigger pair events.
		virtual void onTrigger(physx::PxTriggerPair* a_pairs, 
							   physx::PxU32 a_count)									override final;

		// Sets the boolean which triggers will influence
		inline static void SetTrigger(bool* a_pTriggerBool)
		{
			m_pTrigger = a_pTriggerBool;
		}

	private:
		static bool* m_pTrigger;	// Pointer to bool which triggers will influence
	};
}
