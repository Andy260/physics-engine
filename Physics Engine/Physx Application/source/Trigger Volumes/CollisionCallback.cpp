#include "CollisionCallback.h"

// STL includes
#include <iostream>

// Physx includes
#include <PxRigidActor.h>

using namespace physxApp;

bool* CollisionCallback::m_pTrigger(nullptr);

void CollisionCallback::onConstraintBreak(physx::PxConstraintInfo* a_constraints,
	physx::PxU32 a_count)
{

}

void CollisionCallback::onWake(physx::PxActor** a_actors, physx::PxU32 a_count)
{

}

void CollisionCallback::onSleep(physx::PxActor** a_actors, physx::PxU32 a_count)
{

}

void CollisionCallback::onContact(const physx::PxContactPairHeader& a_pairHeader,
	const physx::PxContactPair* a_pairs, physx::PxU32 a_nbPairs)
{
	for (physx::PxU32 i = 0; i < a_nbPairs; ++i)
	{
		const physx::PxContactPair& contactPair = a_pairs[i];

		if (contactPair.events & physx::PxPairFlag::eNOTIFY_TOUCH_FOUND)
		{
			std::cout << "Collision detected between: ";
			std::cout << a_pairHeader.actors[0]->getName();
			std::cout << a_pairHeader.actors[1]->getName();
			std::cout << std::endl;
		}
	}
}

void CollisionCallback::onTrigger(physx::PxTriggerPair* a_pairs, 
	physx::PxU32 a_count)
{
	for (physx::PxU32 i = 0; i < a_count; ++i)
	{
		physx::PxTriggerPair* pPair = a_pairs + i;
		physx::PxActor* pTriggerActor = pPair->triggerActor;
		physx::PxActor* pOtherActor = pPair->otherActor;

		if (m_pTrigger != nullptr && 
			pOtherActor->getName() == "Player Controller")
		{
			// Inverse trigger bool
			*m_pTrigger = !*m_pTrigger;
		}
	}
}
