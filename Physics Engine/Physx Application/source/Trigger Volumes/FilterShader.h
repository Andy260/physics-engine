#pragma once
#include <PxFiltering.h>

namespace physx { class PxRigidActor; }

namespace physxApp
{
	class PhysxManager;

	class FilterShader
	{
		friend PhysxManager;

	public:
		enum FilterGroup
		{
			eTRIGGER	= (1 << 0),
			ePLAYER		= (1 << 1)
		};

		// Sets an actor's filter
		static void SetActorFilter(physx::PxRigidActor* a_pActor, physx::PxU32 a_filterGroup,
			physx::PxU32 a_filterMask);

		// Sets an actor as a trigger
		static void SetActorAsTrigger(physx::PxRigidActor* a_pActor);

	private:
		FilterShader() {}
		~FilterShader() {}

		static physx::PxFilterFlags PhysxFilterShader(physx::PxFilterObjectAttributes a_attributes0,
			physx::PxFilterData a_filterData0, physx::PxFilterObjectAttributes a_attributes1,
			physx::PxFilterData a_filterData1, physx::PxPairFlags& a_pairFlags,
			const void* a_pConstantBlock, physx::PxU32 a_constantBlockSize);
	};
}
