#include "PhysxVisualiser.h"

// Physx includes
#include <PxPhysicsAPI.h>

// GLM includes
#include <glm\mat4x4.hpp>
#include <glm\vec3.hpp>
#include <glm\ext.hpp>

// OpenGL Engine includes
#include <Gizmos.h>

// Project includes
#include <Physx Wrapper\PhysxManager.h>

using namespace physxApp;

PhysxVisualiser::PhysxVisualiser(const PhysxManager* a_pPhysxManager)
	: m_pPhysxManager(a_pPhysxManager)
{
}

PhysxVisualiser::~PhysxVisualiser()
{
}

void PhysxVisualiser::AddWidget(physx::PxShape* a_pShape, physx::PxRigidActor* a_pActor, 
	bool a_isTrigger)
{
	physx::PxGeometryType::Enum type = a_pShape->getGeometryType();

	switch (type)
	{
	case physx::PxGeometryType::eSPHERE:
		AddSphere(a_pShape, a_pActor, a_isTrigger);
		break;

	case physx::PxGeometryType::ePLANE:
		AddPlane(a_pShape, a_pActor, a_isTrigger);
		break;

	case physx::PxGeometryType::eCAPSULE:
		AddCapsule(a_pShape, a_pActor, a_isTrigger);
		break;

	case physx::PxGeometryType::eBOX:
		AddBox(a_pShape, a_pActor, a_isTrigger);
		break;

	case physx::PxGeometryType::eCONVEXMESH:
		break;

	case physx::PxGeometryType::eTRIANGLEMESH:
		break;

	case physx::PxGeometryType::eHEIGHTFIELD:
		break;

	case physx::PxGeometryType::eGEOMETRY_COUNT:
		break;

	case physx::PxGeometryType::eINVALID:
		break;
	}
}

void PhysxVisualiser::AddBox(physx::PxShape* a_pShape, physx::PxRigidActor* a_pActor, 
	bool a_isTrigger)
{
	// Get the geometry for this Physx collision volume
	physx::PxBoxGeometry geometry;
	float width = 1, height = 1, length = 1;
	
	bool status = a_pShape->getBoxGeometry(geometry);
	if (status)
	{
		width = geometry.halfExtents.x;
		height = geometry.halfExtents.y;
		length = geometry.halfExtents.z;
	}

	// Get the transform for this Physx collision volume
	physx::PxMat44 physxTrans(physx::PxShapeExt::getGlobalPose(*a_pShape, *a_pActor));
	glm::mat4 transform(
		physxTrans.column0.x, physxTrans.column0.y, physxTrans.column0.z, physxTrans.column0.w,
		physxTrans.column1.x, physxTrans.column1.y, physxTrans.column1.z, physxTrans.column1.w,
		physxTrans.column2.x, physxTrans.column2.y, physxTrans.column2.z, physxTrans.column2.w,
		physxTrans.column3.x, physxTrans.column3.y, physxTrans.column3.z, physxTrans.column3.w);

	// Get position of transform
	glm::vec3 position(physxTrans.getPosition().x, physxTrans.getPosition().y, 
		physxTrans.getPosition().z);

	glm::vec3 extents(width, height, length);
	glm::vec4 colour(1, 0, 0, 1);

	// Render geometry
	if (!a_isTrigger)
	{
		Gizmos::addAABBFilled(position, extents, colour, &transform);
	}
	else
	{
		Gizmos::addAABB(position, extents, colour, &transform);
	}
}

void PhysxVisualiser::AddSphere(physx::PxShape* a_pShape, physx::PxRigidActor* a_pActor,
	bool a_isTrigger)
{
	// Get the geometry for this Physx collision volume
	physx::PxSphereGeometry geometry;
	float radius = 1;

	bool status = a_pShape->getSphereGeometry(geometry);
	if (status)
	{
		radius = geometry.radius;
	}

	// Get the transform for this Physx collision volume
	physx::PxMat44 physxTrans(physx::PxShapeExt::getGlobalPose(*a_pShape, *a_pActor));
	glm::mat4 transform(
		physxTrans.column0.x, physxTrans.column0.y, physxTrans.column0.z, physxTrans.column0.w,
		physxTrans.column1.x, physxTrans.column1.y, physxTrans.column1.z, physxTrans.column1.w,
		physxTrans.column2.x, physxTrans.column2.y, physxTrans.column2.z, physxTrans.column2.w,
		physxTrans.column3.x, physxTrans.column3.y, physxTrans.column3.z, physxTrans.column3.w);

	// Get position of transform
	glm::vec3 position(physxTrans.getPosition().x, physxTrans.getPosition().y,
		physxTrans.getPosition().z);

	glm::vec4 colour(0, 0, 1, 1);

	// Render geometry
	Gizmos::addSphere(position, radius, 15, 15, colour, &transform);
}

void PhysxVisualiser::AddPlane(physx::PxShape* a_pShape, physx::PxRigidActor* a_pActor,
	bool a_isTrigger)
{

}

void PhysxVisualiser::AddCapsule(physx::PxShape* a_pShape, physx::PxRigidActor* a_pActor,
	bool a_isTrigger)
{
	// Get the geometry for this Physx collision volume
	physx::PxCapsuleGeometry geometry;
	float radius		= 1.0f;
	float halfheight	= 0.0f;

	bool status = a_pShape->getCapsuleGeometry(geometry);
	if (status)
	{
		radius = geometry.radius;
		halfheight = geometry.halfHeight;
	}

	physx::PxTransform transform = physx::PxShapeExt::getGlobalPose(*a_pShape, 
		*a_pActor);

	physx::PxMat44 m(transform);	// Create rotation matrix from transform
	glm::mat4* pM = (glm::mat4*)(&m);

	// Get the world position form the Physx transform
	physx::PxVec3 physxPos(transform.p);
	glm::vec3 position(physxPos.x, physxPos.y, physxPos.z);

	// Axis for the capsule
	glm::vec4 axis(halfheight, 0, 0, 0);
	// Rotate axis to correct orientation
	axis = *pM * axis;

	glm::vec4 colour(0, 1, 0, 1);

	// Add our two end cap spheres
	Gizmos::addSphere(position + axis.xyz, radius, 10, 10, colour);
	Gizmos::addSphere(position - axis.xyz, radius, 10, 10, colour);
	// Fix the gizmo rotation
	glm::mat4 m2 = glm::rotate(*pM, 11 / 7.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	Gizmos::addCylinderFilled(position, radius, halfheight, 10, colour, &m2);
}

void PhysxVisualiser::Draw()
{
	// Add widgets to represent all the Physx actors within the scene
	const std::vector<physx::PxRigidBody*>& dynamicActors = 
		m_pPhysxManager->GetDynamicActors();
	const std::vector<physx::PxRigidStatic*> & staticActors = 
		m_pPhysxManager->GetStaticActors();
	const std::vector<physx::PxRigidActor*>& triggers =
		m_pPhysxManager->GetTriggers();

	// Render all dynamic actors
	for (auto &dynamicActor : dynamicActors)
	{
		if (dynamicActor == nullptr)
		{
			continue;
		}

		physx::PxU32 nbShapes = dynamicActor->getNbShapes();
		physx::PxShape** shapes = new physx::PxShape*[nbShapes];
		dynamicActor->getShapes(shapes, nbShapes);

		// Render all the shapes in the physx actor
		while (nbShapes--)
		{
			AddWidget(shapes[nbShapes], dynamicActor, false);
		}
		delete[] shapes;
	}

	// Render all static actors
	for (auto &staticActor : staticActors)
	{
		if (staticActor == nullptr)
		{
			continue;
		}

		physx::PxU32 nbShapes = staticActor->getNbShapes();
		physx::PxShape** shapes = new physx::PxShape*[nbShapes];
		staticActor->getShapes(shapes, nbShapes);

		// Render all the shapes in the physx actor
		while (nbShapes--)
		{
			AddWidget(shapes[nbShapes], staticActor, false);
		}
		delete[] shapes;
	}

	// Render all triggers
	for (auto &trigger : triggers)
	{
		if (trigger == nullptr)
		{
			continue;
		}

		physx::PxU32 nbShapes = trigger->getNbShapes();
		physx::PxShape** shapes = new physx::PxShape*[nbShapes];
		trigger->getShapes(shapes, nbShapes);

		// Render all the shapes in the physx actor
		while (nbShapes--)
		{
			AddWidget(shapes[nbShapes], trigger, true);
		}
		delete[] shapes;
	}
}
