#pragma once

// STL includes
#include <vector>

namespace physx { class PxRigidBody; class PxShape; class PxRigidActor; }

namespace physxApp
{
	class PhysxManager;

	class PhysxVisualiser
	{
	public:
		PhysxVisualiser(const PhysxManager* a_pPhysxManager);
		~PhysxVisualiser();

		void Draw();

	private:
		const PhysxManager* m_pPhysxManager;

		void AddWidget(physx::PxShape* a_pShape, physx::PxRigidActor* a_pActor, bool a_isTrigger);

		void AddBox(physx::PxShape* a_pShape, physx::PxRigidActor* a_pActor, bool a_isTrigger);

		void AddSphere(physx::PxShape* a_pShape, physx::PxRigidActor* a_pActor, bool a_isTrigger);

		void AddPlane(physx::PxShape* a_pShape, physx::PxRigidActor* a_pActor, bool a_isTrigger);

		void AddCapsule(physx::PxShape* a_pShape, physx::PxRigidActor* a_Actor, bool a_isTrigger);
	};
}
