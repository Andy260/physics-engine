#include "Game1.h"

// STL includes
#include <iostream>

// OpenGL includes
#include <gl_core_4_4.h>
#include <Gizmos.h>
#include <GameWindow.h>
#include <Input\Keyboard.h>

// GLM includes
#include <glm\vec3.hpp>
#include <glm\ext.hpp>

// Project includes
#include <Trigger Volumes\FilterShader.h>
#include <Trigger Volumes\CollisionCallback.h>

using namespace physxApp;

Game1::Game1() 
	: Game(1280, 720, "Physx Application"),
	m_physxVisualiser(&m_physxManager),
	m_camera(m_pWindow),
	m_ragDoll(&m_physxManager),
	m_physxFluid(&m_physxManager),
	m_playerController(10.0f, 1.0f, &m_physxManager),
	m_simulateFluid(false)
{
}

Game1::~Game1()
{
}

void Game1::SetupPhysx()
{
	m_physxManager.Initialise();

	// Create physics material
	m_PhysicsMaterial = m_physxManager.CreateMaterial(0.5f, 0.5f, 0.5f);

	// Create main physx scene
	m_PhysicsScene = m_physxManager.CreateScene(glm::vec3(0.0f, -10.0f, 0.0f), 1);

	m_physxManager.ConnectToPvd("127.0.0.1", 5425, 100);
}

void Game1::SetupBasicScene()
{
	// Add a box
	float density = 10;
	physx::PxBoxGeometry box(0.5f, 0.5f, 0.5f);
	physx::PxTransform transform(physx::PxVec3(0, 5, 0));
	physx::PxRigidBody* pBox = m_physxManager.AddDynamicBox(transform, box, m_PhysicsMaterial, 
		m_PhysicsScene, density, "Box");

	// Add sphere
	transform.p = physx::PxVec3(0.0f, 10.0f, 0.0f);
	physx::PxSphereGeometry sphere(0.5f);
	physx::PxRigidBody* pSphere = m_physxManager.AddDynamicSphere(transform, 
		sphere, m_PhysicsMaterial, m_PhysicsScene, density, "Sphere");

	// Add trigger volume
	transform.p = physx::PxVec3(-10.0f, 0.0f, -10.0f);
	physx::PxBoxGeometry triggerGeo(5.0f, 1.0f, 5.0f);
	physx::PxRigidActor* pTriggerBox = m_physxManager.CreateTriggerBox(
		transform, triggerGeo, m_PhysicsMaterial, m_PhysicsScene, "Trigger");
}

void Game1::SetupFluid(physx::PxRigidStatic* a_pGround)
{
	// Create fluid bounding volumes
	const physx::PxU32 numShapes = a_pGround->getNbShapes();

	physx::PxVec3 positionOffset(5.0f, 0.0f, 5.0f);

	physx::PxBoxGeometry side1(4.5f, 1.0f, 0.5f);
	physx::PxBoxGeometry side2(0.5f, 1.0f, 4.5f);

	physx::PxTransform pose(physx::PxVec3(0.0f, 0.5f, 4.0f));
	pose.p += positionOffset;
	// Create first container side
	physx::PxRigidStatic* box = m_physxManager.AddStaticBox(pose, side1, m_PhysicsMaterial, 
		m_PhysicsScene, "Fluid Container Side");

	pose	= physx::PxTransform(physx::PxVec3(0.0f, 0.5f, -4.0f));
	pose.p += positionOffset;
	box		= m_physxManager.AddStaticBox(pose, side1, m_PhysicsMaterial, m_PhysicsScene, 
		"Fluid Container Side 1");

	pose	= physx::PxTransform(physx::PxVec3(4.0f, 0.5f, 0.0f));
	pose.p += positionOffset;
	box		= m_physxManager.AddStaticBox(pose, side2, m_PhysicsMaterial, m_PhysicsScene, 
		"Fluid Container Side 2");

	pose	= physx::PxTransform(physx::PxVec3(-4.0f, 0.5f, 0.0f));
	pose.p += positionOffset;
	box		= m_physxManager.AddStaticBox(pose, side2, m_PhysicsMaterial, m_PhysicsScene, 
		"Fluid Container Side 3");

	// Initialise fluid system
	m_physxFluid.Initialise(physx::PxVec3(0.0f, 10.0f, 0.0f) + positionOffset, 
		m_PhysicsScene);
}

void Game1::SetupRagDoll()
{
	physx::PxTransform ragDoll_pose(physx::PxVec3(0.0f, 20.0f, 0.0f), physx::PxQuat(physx::PxHalfPi * 3.0f));
	m_ragDoll.Initialise(ragDoll_pose, 0.1f, m_PhysicsMaterial, m_PhysicsScene);
}

void Game1::SetupPlayerController()
{
	// Initialise player controller
	m_playerController.Initialise(physx::PxExtendedVec3(0.0f, 5.0f, 0.0f), 
		m_PhysicsMaterial, m_PhysicsScene);
}

void Game1::Initialise()
{
	SetupPhysx();

	CollisionCallback::SetTrigger(&m_simulateFluid);

	// Create plane
	physx::PxTransform pose(physx::PxVec3(0.0f, 0.0f, 0.0f),
		physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0.0f, 0.0f, 1.0f)));
	physx::PxRigidStatic* plane = m_physxManager.AddStaticPlane(pose, physx::PxPlaneGeometry(),
		m_PhysicsMaterial, m_PhysicsScene, "Ground Plane");

	SetupBasicScene();
	SetupFluid(plane);
	SetupRagDoll();
	SetupPlayerController();

	// Setup Rendering
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Create Gizmos
	Gizmos::create();

	// Get window size
	int winWidth, winHeight;
	m_pWindow->GetSize(&winWidth, &winHeight);

	// Setup camera
	m_camera.LookAt(glm::vec3(-23, 13, 23), glm::vec3(0, 0, 0),
		glm::vec3(0, 1, 0));
	m_camera.SetProjection(glm::pi<float>() * 0.25f,
		(float)winWidth / (float)winHeight, 0.1f, 1000.0f);
	m_camera.SetSpeed(10.0f);
}

void Game1::Update(const float &a_deltaTime)
{
	m_camera.Update(a_deltaTime);

	m_playerController.Update(a_deltaTime);

	m_physxManager.Update(a_deltaTime);

	if (m_simulateFluid)
	{
		m_physxFluid.Update(a_deltaTime);
	}
}

void Game1::Draw(const float &a_deltaTime)
{
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	Gizmos::clear();

	// Render grid
	Gizmos::addTransform(glm::mat4(1));
	glm::vec4 white(1);
	glm::vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(glm::vec3(-10 + i, 0, 10),
			glm::vec3(-10 + i, 0, -10),
			i == 10 ? white : black);
		Gizmos::addLine(glm::vec3(10, 0, -10 + i),
			glm::vec3(-10, 0, -10 + i),
			i == 10 ? white : black);
	}

	m_physxFluid.Draw();

	m_physxVisualiser.Draw();

	Gizmos::draw(m_camera.GetProjectionViewTransform());
}

void Game1::Shutdown()
{
	
}
