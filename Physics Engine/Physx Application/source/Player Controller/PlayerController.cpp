#include "PlayerController.h"

// Physx includes
#include <PxPhysicsAPI.h>

// Project includes
#include <Physx Wrapper\PhysxManager.h>

// OpenGL Engine includes
#include <Input\Keyboard.h>

using namespace physxApp;

PlayerController::PlayerController(float a_moveSpeed, float a_rotationSpeed, 
	PhysxManager* a_pPhysxManager)
	: m_pPhysxManager(a_pPhysxManager),
	  m_onGround(false),
	  m_moveSpeed(a_moveSpeed),
	  m_roationSpeed(a_rotationSpeed)
{
}

PlayerController::~PlayerController()
{
	m_pController->release();
	m_pControllerManager->release();
}

void PlayerController::Initialise(const physx::PxExtendedVec3& a_startPos, physx::PxMaterial* a_pMaterial, physx::PxScene* a_pScene)
{
	m_pControllerManager = PxCreateControllerManager(*a_pScene);

	// Create controller descriptor
	physx::PxCapsuleControllerDesc desc;
	desc.height			= 1.6f;
	desc.radius			= 0.4f;
	desc.material		= a_pMaterial;
	desc.reportCallback = &m_hitReport;
	desc.density		= 10.0f;
	desc.position.set(0.0f, 0.0f, 0.0f);

	// Create the player controller
	m_pController = m_pControllerManager->createController(desc);
	m_pController->setPosition(a_startPos);

	//m_gravity	= physx::PxVec3(0.0f, 10.0f, 0.0f);
	m_gravity = a_pScene->getGravity();
	m_gravity.y *= -1.0f;

	m_hitReport.ClearPlayerContactNormal();

	// Add actor to physx manager, to render it
	physx::PxRigidDynamic* pRigidActor = m_pController->getActor();
	m_pPhysxManager->m_dynamicActors.push_back(pRigidActor);

	pRigidActor->setName("Player Controller");
}

void PlayerController::Update(float a_deltaTime)
{
	physx::PxVec3 velocity(0.0f, 0.0f, 0.0f);

	// Check if we have a contact normal. 
	// If y is greater than .3 we assume this is solid ground. 
	// This is a rather primitive way to do this.
	if (m_hitReport.GetPlayerContactNormal().y > 0.3f &&
		m_hitReport.IsOnGround())
	{
		velocity.y		= 0.0f;
		m_onGround		= true;
	}
	else
	{
		velocity.y		+= m_gravity.y * a_deltaTime;
		m_onGround		 = false;
	}

	m_hitReport.ClearPlayerContactNormal();

	// Get input
	HandleInput(velocity, a_deltaTime);

	// Move object out of the way, if hitting one
	physx::PxRigidDynamic* pRigidDynamic = m_hitReport.GetLastHit();
	if (pRigidDynamic != nullptr)
	{
		physx::PxVec3 moveVelocity(velocity);

		if (!m_hitReport.IsOnGround())
		{
			moveVelocity *= -1;
		}

		pRigidDynamic->addForce(velocity * pRigidDynamic->getMass(),
			physx::PxForceMode::eIMPULSE);
	}

	// Move controller
	float minDistance = 0.001f;
	physx::PxControllerFilters filter;
	m_pController->move(m_rotation.rotate(velocity), minDistance,
		a_deltaTime, filter);
}

void PlayerController::HandleInput(physx::PxVec3& a_velocity, float a_deltaTime)
{
	// Handle controller movement
	if (Keyboard::IsKeyDown(Keys::KEY_LEFT))
	{
		a_velocity.x += m_moveSpeed * a_deltaTime;
	}
	if (Keyboard::IsKeyDown(Keys::KEY_RIGHT))
	{
		a_velocity.x -= m_moveSpeed * a_deltaTime;
	}

	if (Keyboard::IsKeyDown(Keys::KEY_UP))
	{
		a_velocity.z += m_moveSpeed * a_deltaTime;
	}
	if (Keyboard::IsKeyDown(Keys::KEY_DOWN))
	{
		a_velocity.z -= m_moveSpeed * a_deltaTime;
	}
}
