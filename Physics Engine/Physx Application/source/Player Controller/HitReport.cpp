#include "HitReport.h"

// Physx includes
#include <PxRigidActor.h>
#include <PxRigidDynamic.h>

using namespace physxApp;

HitReport::HitReport()
	: m_onGround(false)
{

}

HitReport::~HitReport()
{

}

void HitReport::onShapeHit(const physx::PxControllerShapeHit& a_hit)
{
	// Get colliding actor
	physx::PxRigidActor* pActor = a_hit.shape->getActor();
	
	// Store normal of colliding actor
	m_playerContactNormal = a_hit.worldNormal;

	if (pActor->getName() == "Ground Plane")
	{
		m_onGround = true;
	}
	else
	{
		m_onGround = false;
	}

	// Try to cast to dynamic actor
	physx::PxRigidDynamic* pDynamicActor = pActor->is<physx::PxRigidDynamic>();
	if (pDynamicActor)
	{
		m_pLastHit = pDynamicActor;
	}
	else
	{
		m_pLastHit = nullptr;
	}
}

void HitReport::onControllerHit(const physx::PxControllersHit &a_hit)
{

}

void HitReport::onObstacleHit(const physx::PxControllerObstacleHit & a_hit)
{

}
