#pragma once

// Physx includes
#include <characterkinematic\PxControllerManager.h>
#include <characterkinematic\PxCapsuleController.h>
#include <foundation\PxVec3.h>
#include <foundation\PxQuat.h>

// Project includes
#include "HitReport.h"

namespace physxApp
{
	class PhysxManager;

	class PlayerController
	{
	public:
		// Constructs a new player controller, with a given move speed, 
		// rotation speed (not used) and physx manager, to render with
		PlayerController(float a_moveSpeed, float a_rotationSpeed,
						 PhysxManager* a_pPhysxManager);
		~PlayerController();

		void Initialise(const physx::PxExtendedVec3& a_startPos, 
						physx::PxMaterial* a_pMaterial, 
						physx::PxScene* a_pScene);

		void Update(float a_deltaTime);

	private:
		PhysxManager* m_pPhysxManager;

		HitReport m_hitReport;

		physx::PxControllerManager* m_pControllerManager;

		physx::PxController* m_pController;

		physx::PxQuat m_rotation;

		physx::PxVec3 m_gravity;

		bool m_onGround;
		float m_moveSpeed;
		float m_roationSpeed;

		void HandleInput(physx::PxVec3& a_velocity, float a_deltaTime);
	};
}
