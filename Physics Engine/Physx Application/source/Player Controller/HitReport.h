#pragma once
#include <characterkinematic\PxController.h>

namespace physxApp
{
	class HitReport : public physx::PxUserControllerHitReport
	{
	public:
		HitReport();
		~HitReport();

		// Called when current controller hits a shape
		void onShapeHit(const physx::PxControllerShapeHit & a_hit)		override final;

		// Called when current controller hits another controller.
		void onControllerHit(const physx::PxControllersHit &a_hit)		override final;

		// Called when current controller hits a user-defined obstacle.
		void onObstacleHit(const physx::PxControllerObstacleHit &a_hit)	override final;

		inline physx::PxVec3 GetPlayerContactNormal() const 
		{ 
			return m_playerContactNormal; 
		};

		inline void ClearPlayerContactNormal()
		{ 
			m_playerContactNormal = physx::PxVec3(0, 0, 0); 
		};

		inline bool IsOnGround() const
		{
			return m_onGround;
		}

		inline physx::PxRigidDynamic* GetLastHit()
		{
			physx::PxRigidDynamic* pLastHit = m_pLastHit;
			m_pLastHit = nullptr;

			return pLastHit;
		}

	private:
		physx::PxVec3 m_playerContactNormal;	// The normal of the collision
		bool m_onGround;						// Flags if player controller is currently on the ground
		physx::PxRigidDynamic* m_pLastHit;		// Pointer to dynamic actor which was last hit
	};
}
