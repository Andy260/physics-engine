#version 410

in vec4 position;
in vec4 colour;
in vec2 texCoord;

out vec4 fPosition;
out vec2 fTexCoord;
out vec4 fColour;

uniform mat4 projectionView;

void main()
{
	fPosition 	= position;
	fColour 	= colour;
	fTexCoord 	= texCoord;
	
	gl_Position = projectionView * position;
}