#pragma once

class RenderTarget
{
private:
	unsigned int m_fbo, m_fboTexture, m_depthBuffer;
	int m_width, m_height;

public:
	RenderTarget(const int& a_width, const int& a_height);
	~RenderTarget();

	void AttachTexture();

	void Bind();

	void BindAsTexture(const int& a_unit);

	static void BindBackBuffer();

	int GetWidth() const { return m_width; }
	int GetHeight() const { return m_height; }
};