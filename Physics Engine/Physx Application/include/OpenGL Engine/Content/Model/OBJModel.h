#pragma once
#include <tinyobjloader\tiny_obj_loader.h>
#include <glm\glm.hpp>
#include <vector>

class OBJModel
{
private:
	struct GLHandle
	{
		unsigned int m_VAO;
		unsigned int m_VBO;
		unsigned int m_IBO;
		unsigned int m_indexCount;
	};
	std::vector<GLHandle> m_glHandles;
	std::vector<tinyobj::shape_t> m_shapes;
	std::vector<tinyobj::material_t> m_materials;
	glm::mat4 m_worldTransform;

public:
	OBJModel() {}
	~OBJModel();

	void Load(const char* a_filePath);
	void Draw();
};