#pragma once

// STL includes
#include <vector>

// GLM includes
#include <glm\vec3.hpp>

namespace physics
{
	class Engine;
	class Scene;
	class BoundingVolume;
	class RigidBody;

	class CollisionHandler
	{
		friend Engine;

	private:
		CollisionHandler(const std::vector<Scene*>& a_scenes)
			: m_scenes(a_scenes) {};
		~CollisionHandler() {}

		const std::vector<Scene*>& m_scenes;	// Reference to all scenes to check for collisions

		// Checks all collisions on every scene
		void CheckCollisions();

		// Returns the combined elasticity of two rigid bodies
		float GetCombinedElasticity(float a_elasticity, float a_elasticity1);

#pragma region Collision Tests
		// Returns true if both planes are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void PlaneToPlane(BoundingVolume* a_pVolume,
			BoundingVolume* a_pVolume1);

		// Returns true if a given plane and sphere are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void PlaneToSphere(BoundingVolume* a_pVolume,
			BoundingVolume* a_pVolume1);
		
		// Returns true if a given plane and OBB are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void PlaneToOBB(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given plane and Capsule are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void PlaneToCapsule(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given plane and ray are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void PlaneToRay(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given sphere and secondary sphere are colliding 
		// with each other
		// Will return false if both types aren't that of the given objects
		static void SphereToSphere(BoundingVolume* a_pSphere, BoundingVolume* a_pSphere1);

		// Returns true if a given sphere and AABB are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void SphereToAABB(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given sphere and OBB are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void SphereToOBB(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given sphere and Capsule are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void SphereToCapsule(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given sphere and Ray are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void SphereToRay(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given AABB and Plane are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void AABBToPlane(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if both AABBs are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void AABBToAABB(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given AABB and OBB are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void AABBToOBB(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given AABB and Capsule are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void AABBToCapsule(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given AABB and Ray are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void AABBToRay(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if both OBBs are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void OBBToOBB(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given OBB and Capsule are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void OBBToCapsule(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given OBB and ray are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void OBBToRay(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if both capsules are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void CapsuleToCapsule(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if a given capsule and ray are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void CapsuleToRay(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);

		// Returns true if rays are colliding with each other
		// Will return false if both types aren't that of the given objects
		static void RayToRay(BoundingVolume* a_pVolume, BoundingVolume* a_pVolume1);
#pragma endregion

#pragma region Duplicate Collision Test Functions
		inline static void AABBToSphere(BoundingVolume* a_pVolume,
											 BoundingVolume* a_pVolume1)
		{
			SphereToAABB(a_pVolume, a_pVolume1);
		}

		inline static void PlaneToAABB(BoundingVolume* a_pVolume,
											BoundingVolume* a_pVolume1)
		{
			AABBToPlane(a_pVolume, a_pVolume1);
		}

		inline static void SphereToPlane(BoundingVolume* a_pVolume,
											  BoundingVolume* a_pVolume1)
		{
			PlaneToSphere(a_pVolume, a_pVolume1);
		}

		inline static void OBBToPlane(BoundingVolume* a_pVolume,
										   BoundingVolume* a_pVolume1)
		{
			PlaneToOBB(a_pVolume, a_pVolume1);
		}

		inline static void OBBToSphere(BoundingVolume* a_pVolume,
											BoundingVolume* a_pVolume1)
		{
			SphereToSphere(a_pVolume, a_pVolume1);
		}

		inline static void OBBToAABB(BoundingVolume* a_pVolume,
										  BoundingVolume* a_pVolume1)
		{
			AABBToOBB(a_pVolume, a_pVolume1);
		}

		inline static void CapsuleToPlane(BoundingVolume* a_pVolume,
											   BoundingVolume* a_pVolume1)
		{
			PlaneToCapsule(a_pVolume, a_pVolume1);
		}

		inline static void CapsuleToSphere(BoundingVolume* a_pVolume,
												BoundingVolume* a_pVolume1)
		{
			SphereToCapsule(a_pVolume, a_pVolume1);
		}

		inline static void CapsuleToAABB(BoundingVolume* a_pVolume,
											  BoundingVolume* a_pVolume1)
		{
			AABBToCapsule(a_pVolume, a_pVolume1);
		}

		inline static void CapsuleToOBB(BoundingVolume* a_pVolume,
											 BoundingVolume* a_pVolume1)
		{
			OBBToCapsule(a_pVolume, a_pVolume1);
		}

		inline static void RayToPlane(BoundingVolume* a_pVolume,
										   BoundingVolume* a_pVolume1)
		{
			PlaneToRay(a_pVolume, a_pVolume1);
		}

		inline static void RayToSphere(BoundingVolume* a_pVolume,
											BoundingVolume* a_pVolume1)
		{
			SphereToRay(a_pVolume, a_pVolume1);
		}

		inline static void RayToAABB(BoundingVolume* a_pVolume,
										  BoundingVolume* a_pVolume1)
		{
			AABBToRay(a_pVolume, a_pVolume1);
		}

		inline static void RayToOBB(BoundingVolume* a_pVolume,
										 BoundingVolume* a_pVolume1)
		{
			OBBToRay(a_pVolume, a_pVolume1);
		}

		inline static void RayToCapsule(BoundingVolume* a_pVolume,
											 BoundingVolume* a_pVolume1)
		{
			CapsuleToRay(a_pVolume, a_pVolume1);
		}
#pragma endregion
	};
}
