#include "CollisionHandler.h"

// STL includes
#include <iostream>

// Bounding Volume includes
#include <Bounding Volumes\BoundingVolume.h>
#include <Bounding Volumes\Plane.h>
#include <Bounding Volumes\Sphere.h>
#include <Bounding Volumes\AABB.h>

// Dependacy includes
#include <Physics Objects\RigidBody.h>
#include <Scene.h>

// External library includes
#include <glm\geometric.hpp>

using namespace physics;

void CollisionHandler::CheckCollisions()
{
	// Defines a collision test function
	typedef void(*collisionFunc) (BoundingVolume*, BoundingVolume*);

	// Array containing all collision test functions within this class
	static collisionFunc collisionFuncArray[] =
	{
		&CollisionHandler::PlaneToPlane,
		&CollisionHandler::PlaneToSphere,
		&CollisionHandler::PlaneToAABB,
		&CollisionHandler::PlaneToOBB,
		&CollisionHandler::PlaneToCapsule,
		&CollisionHandler::PlaneToRay,

		&CollisionHandler::SphereToPlane,
		&CollisionHandler::SphereToSphere,
		&CollisionHandler::SphereToAABB,
		&CollisionHandler::SphereToOBB,
		&CollisionHandler::SphereToCapsule,
		&CollisionHandler::SphereToRay,

		&CollisionHandler::AABBToPlane,
		&CollisionHandler::AABBToSphere,
		&CollisionHandler::AABBToAABB,
		&CollisionHandler::AABBToOBB,
		&CollisionHandler::AABBToCapsule,
		&CollisionHandler::AABBToRay,

		&CollisionHandler::OBBToPlane,
		&CollisionHandler::OBBToSphere,
		&CollisionHandler::OBBToAABB,
		&CollisionHandler::OBBToOBB,
		&CollisionHandler::OBBToCapsule,
		&CollisionHandler::OBBToRay,

		&CollisionHandler::CapsuleToPlane,
		&CollisionHandler::CapsuleToSphere,
		&CollisionHandler::CapsuleToAABB,
		&CollisionHandler::CapsuleToOBB,
		&CollisionHandler::CapsuleToCapsule,
		&CollisionHandler::CapsuleToRay,

		&CollisionHandler::RayToPlane,
		&CollisionHandler::RayToSphere,
		&CollisionHandler::RayToAABB,
		&CollisionHandler::RayToOBB,
		&CollisionHandler::RayToCapsule,
		&CollisionHandler::RayToRay,
	};

	for (auto &i : m_scenes)
	{
		const std::vector<RigidBody*>& actorsList = i->GetActors();

		unsigned int actorCount = actorsList.size();
		int typeCount = BoundingVolume::GetTypeCount();

		for (unsigned int outer = 0; outer < actorCount; ++outer)
		{
			for (unsigned int inner = 0; inner < actorCount; ++inner)
			{
				// Don't check collision against same actor
				if (outer == inner)
				{
					continue;
				}

				// Get objects to check a collision against
				BoundingVolume* object1 = actorsList[outer]->GetBoundingVolume();
				BoundingVolume* object2 = actorsList[inner]->GetBoundingVolume();

				// Get the bounding volume types
				int object1Type = object1->GetIntType();
				int object2Type = object2->GetIntType();

				// Use the appropriate function
				int functionIndex = (object1Type * typeCount) + object2Type;
				collisionFunc collisionFuncPtr = collisionFuncArray[functionIndex];
				if (collisionFuncPtr != nullptr)
				{
					collisionFuncPtr(object1, object2);
				}
			}
		}
	}
}

void CollisionHandler::PlaneToPlane(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::PlaneToSphere(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// Try to cast given objects into their child classes
	Sphere* pSphere = nullptr;
	Plane* pPlane = nullptr;

	pSphere = dynamic_cast<Sphere*>(a_pVolume);
	pPlane = dynamic_cast<Plane*>(a_pVolume1);

	if (pSphere == nullptr &&
		pPlane == nullptr)
	{
		pSphere = dynamic_cast<Sphere*>(a_pVolume1);
		pPlane = dynamic_cast<Plane*>(a_pVolume);
	}

	glm::vec3 planeNormal = pPlane->GetNormal();
	glm::vec3 spherePos = pSphere->GetPosition();
	float planeDistOrigin = pPlane->GetDistanceToOrigin();

	glm::vec3 collisionNormal = planeNormal;
	float sphereToPlane = glm::dot(spherePos, planeNormal) -
		planeDistOrigin;

	// Flip normal if object is behind it
	if (sphereToPlane < 0.0f)
	{
		collisionNormal *= -1.0f;
		sphereToPlane *= -1.0f;
	}
	// Intersection between sphere and plane
	float intersection = pSphere->GetRadius() - sphereToPlane;

	if (intersection > 0.0f)
	{
		RigidBody* pRigidBody = pSphere->GetRigidBody();

		// Find collision point
		glm::vec3 planeNormal = pPlane->GetNormal();

		if (sphereToPlane < 0.0f)
		{
			planeNormal *= 1.0f;	// Flip normal if object is behind plane
		}

		glm::vec3 forceVector = -1.0f * pRigidBody->GetMass() * planeNormal
			* glm::abs(glm::dot(planeNormal, pRigidBody->GetVelocity()));

		pRigidBody->ApplyForce((2.0f * -forceVector) * 0.6f);
		pRigidBody->SetPosition((collisionNormal * intersection * 0.5f)
			+ pRigidBody->GetPosition());

		bool test = false;
	}

	// Given objects aren't expected type - dynamic cast failure
}

void CollisionHandler::PlaneToOBB(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::PlaneToCapsule(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::PlaneToRay(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::SphereToSphere(BoundingVolume* a_pSphere,
	BoundingVolume* a_pSphere1)
{
	// Try to cast given objects into their child classes
	Sphere* pSphere		= dynamic_cast<Sphere*>(a_pSphere);
	Sphere* pSphere1	= dynamic_cast<Sphere*>(a_pSphere1);

	// Check if a collision has occured, if cast succeeded
	if (pSphere != nullptr &&
		pSphere1 != nullptr)
	{
		// Check if a collision occured
		glm::vec3 delta		= pSphere1->GetPosition() - pSphere->GetPosition();
		float distance		= glm::length(delta);
		float intersection	= pSphere->GetRadius() + pSphere1->GetRadius() -
								distance;

		// Construct collision object if collision is occuring
		if (intersection > 0)
		{
			RigidBody* pRigidBody = pSphere->GetRigidBody();
			RigidBody* pRigidBody1 = pSphere1->GetRigidBody();

			glm::vec3 collisionNormal = glm::normalize(delta);

			glm::vec3 collisionVelocity = pRigidBody->GetVelocity() -
				pRigidBody1->GetVelocity();

			glm::vec3 collisionVector = collisionNormal *
				(glm::dot(collisionVelocity,
				collisionNormal));

			glm::vec3 collisionForce = collisionVector * 1.0f / (1.0f /
				pRigidBody->GetMass() +
				1.0f / pRigidBody1->GetMass());

			glm::vec3 collisionSeperation = collisionNormal * intersection *
				0.5f;

			// Use newtons third law to apply collision forces to colliding bodies
			pRigidBody->ApplyForceToActor((2.0f * collisionForce) * 0.6f, pRigidBody1);

			// Move rigid body out of collision
			if (pRigidBody->GetType() != RigidBody::Type::STATIC)
			{
				pRigidBody->SetPosition(pRigidBody->GetPosition() -
					collisionSeperation);
			}
			if (pRigidBody1->GetType() != RigidBody::Type::STATIC)
			{
				pRigidBody1->SetPosition(pRigidBody1->GetPosition() +
					collisionSeperation);
			}
		}
	}

	// Given objects aren't expected type - dynamic cast failure
}

void CollisionHandler::SphereToAABB(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// Try to cast given objects into their child classes
	AABB* pAABB = nullptr;
	Sphere* pSphere = nullptr;

	pAABB = dynamic_cast<AABB*>(a_pVolume);
	pSphere = dynamic_cast<Sphere*>(a_pVolume1);

	if (pAABB == nullptr &&
		pSphere == nullptr)
	{
		pAABB = dynamic_cast<AABB*>(a_pVolume1);
		pSphere = dynamic_cast<Sphere*>(a_pVolume);
	}

	if (pAABB == nullptr || pSphere == nullptr)
	{
		// Cast failure
		return;
	}

	float distSquared = pow(pSphere->GetRadius(), 2.0f);

	glm::vec3 aabb_pos		= pAABB->GetPosition();
	glm::vec3 sphere_pos	= pSphere->GetPosition();

	glm::vec3 delta = sphere_pos - aabb_pos;

	glm::vec3 aabb_dims = pAABB->GetDimentions();

	glm::vec3 aabb_max(aabb_pos.x + aabb_dims.x, aabb_pos.y + aabb_dims.y, 
		aabb_pos.z + aabb_dims.z);
	glm::vec3 aabb_min(aabb_pos.x - aabb_dims.x, aabb_pos.y - aabb_dims.y,
		aabb_pos.z - aabb_dims.z);

	// Check if a collision occured
	if (sphere_pos.x > aabb_max.x)
	{
		distSquared -= pow(sphere_pos.x - aabb_max.x, 2.0f);
	}
	else if (sphere_pos.x < aabb_min.x)
	{
		distSquared -= pow(sphere_pos.x - aabb_min.x, 2.0f);
	}

	if (sphere_pos.y > aabb_max.y)
	{
		distSquared -= pow(sphere_pos.x - aabb_max.x, 2.0f);
	}
	else if (sphere_pos.y < aabb_min.y)
	{
		distSquared -= pow(sphere_pos.y - aabb_min.y, 2.0f);
	}

	if (sphere_pos.z > aabb_max.z)
	{
		distSquared -= pow(sphere_pos.y - aabb_max.y, 2.0f);
	}
	else if (sphere_pos.z < aabb_min.z)
	{
		distSquared -= pow(sphere_pos.z - aabb_min.z, 2.0f);
	}

	if (distSquared > 0.0f)
	{
		RigidBody* pSphereBody	= pSphere->GetRigidBody();
		RigidBody* pAABBBody	= pAABB->GetRigidBody();

		glm::vec3 collisionNormal = glm::normalize(delta);

		glm::vec3 relativeVelocity = pAABBBody->GetVelocity() - 
			pSphereBody->GetVelocity();
		
		glm::vec3 collisionVector = collisionNormal * 
			std::abs(glm::dot(relativeVelocity, collisionNormal));
		
		glm::vec3 forceVector = collisionVector * 1.0f / 
			(1.0f / pAABBBody->GetMass() + 1.0f / pSphereBody->GetMass());

		pAABBBody->ApplyForceToActor((forceVector * 2.0f) * 0.6f, 
			pSphereBody);

		glm::vec3 seperationVector = collisionNormal * distSquared * 0.7f;

		if (pAABBBody->GetType() != RigidBody::Type::STATIC)
		{
			pAABBBody->SetPosition(aabb_pos - seperationVector);
		}
		if (pSphereBody->GetType() != RigidBody::Type::STATIC)
		{
			pSphereBody->SetPosition(sphere_pos + seperationVector);
		}
	}
}

void CollisionHandler::SphereToOBB(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::SphereToCapsule(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::SphereToRay(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::AABBToPlane(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// Try to cast given objects into their child classes
	AABB* pAABB		= nullptr;
	Plane* pPlane	= nullptr;

	pAABB	= dynamic_cast<AABB*>(a_pVolume);
	pPlane	= dynamic_cast<Plane*>(a_pVolume1);

	if (pAABB == nullptr &&
		pPlane == nullptr)
	{
		pAABB	= dynamic_cast<AABB*>(a_pVolume1);
		pPlane	= dynamic_cast<Plane*>(a_pVolume);
	}

	if (pAABB == nullptr ||
		pPlane == nullptr)
	{
		// Casting failure
		return;
	}

	glm::vec3 collisionNormal = pPlane->GetNormal();

	glm::vec3 aabbPos = pAABB->GetPosition();
	glm::vec3 aabbDims = pAABB->GetDimentions() / 2.0f;

	float collision = 0.0f;

	glm::vec3 points[8] =
	{
		glm::vec3(aabbPos.x + aabbDims.x, aabbPos.y + aabbDims.y, aabbPos.z + aabbDims.z),
		glm::vec3(aabbPos.x - aabbDims.x, aabbPos.y - aabbDims.y, aabbPos.z - aabbDims.z),
		glm::vec3(aabbPos.x + aabbDims.x, aabbPos.y - aabbDims.y, aabbPos.z + aabbDims.z),
		glm::vec3(aabbPos.x - aabbDims.x, aabbPos.y - aabbDims.y, aabbPos.z + aabbDims.z),
		glm::vec3(aabbPos.x + aabbDims.x, aabbPos.y - aabbDims.y, aabbPos.z - aabbDims.z),
		glm::vec3(aabbPos.x - aabbDims.x, aabbPos.y + aabbDims.y, aabbPos.z - aabbDims.z),
		glm::vec3(aabbPos.x + aabbDims.x, aabbPos.y + aabbDims.y, aabbPos.z - aabbDims.z),
		glm::vec3(aabbPos.x - aabbDims.x, aabbPos.y + aabbDims.y, aabbPos.z + aabbDims.z)
	};

	float dotPoints[8];

	// Dot product again each point of the AABB
	for (unsigned int i = 0; i < 8; ++i)
	{
		dotPoints[i] = glm::dot(points[i], pPlane->GetNormal()) 
			- pPlane->GetDistanceToOrigin();
	}

	// Check if any points of the AABB collide with the plane
	for (unsigned int i = 0; i < 8; ++i)
	{
		if (dotPoints[i] < 0.0f)
		{
			RigidBody* pRigidBody = pAABB->GetRigidBody();
			
			// Find the point in where the collision occured
			glm::vec3 planeNormal = pPlane->GetNormal();
			
			// Calculate force vector
			glm::vec3 forceVector = 1.0f * pRigidBody->GetMass() * planeNormal * 
				std::abs(glm::dot(planeNormal, pRigidBody->GetVelocity()));
			
			// Apply force
			pRigidBody->ApplyForce((2.0f * forceVector) * 0.5f);
			
			// Move object out of collision
			pRigidBody->SetPosition(pRigidBody->GetPosition() + 
				(collisionNormal * collision));

			// Collision occured
			return;
		}
	}
}

void CollisionHandler::AABBToAABB(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// Try to cast given objects into their child classes
	AABB* pAABB = dynamic_cast<AABB*>(a_pVolume);
	AABB* pAABB1 = dynamic_cast<AABB*>(a_pVolume1);

	if (pAABB == nullptr ||
		pAABB1 == nullptr)
	{
		// Dynamic cast failure
		return;
	}

	glm::vec3 aabb_pos = pAABB->GetPosition();
	glm::vec3 aabb1_pos = pAABB1->GetPosition();

	glm::vec3 aabb_max = pAABB->GetMax();
	glm::vec3 aabb1_max = pAABB1->GetMax();

	glm::vec3 aabb_dims = pAABB->GetDimentions();
	glm::vec3 aabb1_dims = pAABB1->GetDimentions();

	glm::vec3 delta = pAABB1->GetPosition() - pAABB->GetPosition();

	glm::vec3 aabb_overlap = aabb_max - aabb1_pos;
	glm::vec3 aabb1_overlap = aabb1_max - aabb_pos;

	// Check if a collision occured
	if (aabb_pos.x < aabb1_max.x &&
		aabb_pos.x + aabb_dims.x > aabb1_pos.x &&
		aabb_pos.y < aabb1_max.y &&
		aabb_pos.y + aabb_dims.y > aabb1_pos.y &&
		aabb_pos.z < aabb1_max.z &&
		aabb_pos.z + aabb_dims.z > aabb1_pos.z)
	{
		RigidBody* pRigidBody = pAABB->GetRigidBody();
		RigidBody* pRigidBody1 = pAABB1->GetRigidBody();

		glm::vec3 collisionNormal = glm::normalize(delta);

		glm::vec3 relativeVelocity = pRigidBody->GetVelocity()
			- pRigidBody1->GetVelocity();

		glm::vec3 collisionVector = collisionNormal *
			std::abs(glm::dot(relativeVelocity, collisionNormal));

		glm::vec3 forceVector = collisionVector * 1.0f /
			(1.0f / pRigidBody->GetMass() + 1.0f / pRigidBody1->GetMass());

		// Apply collision response
		pRigidBody->ApplyForceToActor((forceVector * 2.0f) * 0.5f, pRigidBody1);

		float min = glm::min(aabb_overlap.x, glm::min(aabb_overlap.y,
			aabb_overlap.z));

		// Seperate the rigid bodies
		glm::vec3 seperationVector = (collisionNormal * min) * 0.001f;
		pRigidBody->SetPosition(pRigidBody->GetPosition() + seperationVector);
		pRigidBody1->SetPosition(pRigidBody1->GetPosition() + seperationVector);
	}
}

void CollisionHandler::AABBToOBB(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	
}

void CollisionHandler::AABBToCapsule(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::AABBToRay(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::OBBToOBB(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::OBBToCapsule(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::OBBToRay(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::CapsuleToCapsule(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::CapsuleToRay(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}

void CollisionHandler::RayToRay(BoundingVolume* a_pVolume,
	BoundingVolume* a_pVolume1)
{
	// TODO: Implement this function
	//return Collision();
}
