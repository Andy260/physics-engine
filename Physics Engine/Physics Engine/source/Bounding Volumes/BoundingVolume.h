#pragma once

namespace physics
{
	class RigidBody;

	class BoundingVolume
	{
	public:
		// Defines a type of a bounding volume
		enum class Type : int
		{
			PLANE	= 0,
			SPHERE	= 1,
			AABB	= 2,
			OBB		= 3,
			CAPSULE = 4,
			RAY		= 5,
			COUNT	= RAY + 1
		};

		BoundingVolume(Type a_type)
			: m_type(a_type) {}

		BoundingVolume(Type a_type, RigidBody* a_pRigidBody)
			: m_type(a_type), m_pRigidBody(a_pRigidBody) {}

		virtual ~BoundingVolume() {}

		// Returns the enumerable type of this bounding volume
		inline Type GetType() const { return m_type; }

		// Returns the underlaying integer of this bounding volume's type
		inline int GetIntType() const 
		{
			return static_cast<int>(m_type);
		}

		// Returns the total possible number of bounding volume types
		inline static int GetTypeCount()
		{
			Type count = Type::COUNT;

			return static_cast<int>(count);
		}

		// Returns which rigid body this bounding volume to attached to
		inline RigidBody* GetRigidBody() const { return m_pRigidBody; }
		inline void SetRigidBody(RigidBody* a_pRigidBody) { m_pRigidBody = a_pRigidBody; }

	private:
		Type m_type;				// Stores which type this bounding volume is
		RigidBody* m_pRigidBody;	// Pointer to which rigidbody this volume is attached to
	};
}
