#pragma once
#include <Bounding Volumes\BoundingVolume.h>
#include <glm\vec3.hpp>

namespace physics
{
	class AABB : public BoundingVolume
	{
	public:
		AABB(const glm::vec3& a_pos, const glm::vec3& a_dims) 
			: BoundingVolume(BoundingVolume::Type::AABB), 
			  m_position(a_pos), m_dimensions(a_dims) {}
		~AABB() override {}

		glm::vec3 GetMin() const;
		glm::vec3 GetMax() const;

		glm::vec3 GetPosition() const;
		inline void SetPosition(const glm::vec3& a_pos) { m_position = a_pos; }

		inline glm::vec3 GetDimentions() const { return m_dimensions; }
		inline void SetDimentions(const glm::vec3& a_dims) { m_dimensions = a_dims; }

	private:
		glm::vec3 m_position;		// Local position of attached rigid body (world space if none attached)
		glm::vec3 m_dimensions;		// The size of the box along each respective axis
	};
}
