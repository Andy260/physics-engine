#pragma once
#include <Bounding Volumes\BoundingVolume.h>
#include <glm\vec3.hpp>
#include <glm\geometric.hpp>

namespace physics
{
	class Plane : public BoundingVolume
	{
	public:
		Plane(const glm::vec3& a_normal, float a_distToNormal)
			: BoundingVolume(BoundingVolume::Type::PLANE), m_normal(a_normal),
			  m_distToOrigin(a_distToNormal) {}
		~Plane() override {}

		inline glm::vec3 GetNormal() const { return m_normal; }
		inline void SetNormal(const glm::vec3& a_normal) { m_normal = a_normal; }

		inline float GetDistanceToOrigin() const { return m_distToOrigin; }
		inline void SetDistanceToOrigin(float a_distToOrigin) 
		{ 
			m_distToOrigin = glm::normalize(a_distToOrigin); 
		}

	private:
		glm::vec3 m_normal;
		float m_distToOrigin;
	};
}
