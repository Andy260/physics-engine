#include "Sphere.h"
#include <Physics Objects\RigidBody.h>

using namespace physics;

glm::vec3 Sphere::GetPosition() const
{
	RigidBody* pRigidBody = GetRigidBody();

	if (pRigidBody == nullptr)
	{
		return m_position;
	}

	return m_position + pRigidBody->GetPosition();
}
