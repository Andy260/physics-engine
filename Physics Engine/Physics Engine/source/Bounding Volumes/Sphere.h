#pragma once
#include <Bounding Volumes\BoundingVolume.h>
#include <glm\vec3.hpp>

namespace physics
{
	class Sphere : public BoundingVolume
	{
	public:
		Sphere(const glm::vec3& a_direction, float a_radius) 
			: BoundingVolume(BoundingVolume::Type::SPHERE), m_radius(a_radius) {}

		inline float GetRadius() const { return m_radius; }
		inline void SetRadius(float a_radius) { m_radius = a_radius; }

		glm::vec3 GetPosition() const;
		inline void SetPosition(const glm::vec3& a_position) { m_position = a_position; }

	private:
		glm::vec3 m_position;	// Current position of this sphere, in the scene
		float m_radius;			// Radius of this sphere, in scene units
	};
}
