#include "AABB.h"

// Dependancy includes
#include <Physics Objects\RigidBody.h>

using namespace physics;

glm::vec3 AABB::GetPosition() const
{
	RigidBody* pRigidBody = GetRigidBody();

	if (pRigidBody == nullptr)
	{
		return m_position;
	}

	return m_position + pRigidBody->GetPosition();
}

glm::vec3 AABB::GetMin() const
{
	glm::vec3 position		= GetPosition();
	glm::vec3 dimentions	= GetDimentions();

	return glm::vec3(position.x - (dimentions.x * 0.9f),
		position.y - (dimentions.y * 0.9f),
		position.z - (dimentions.z * 0.9f));
}

glm::vec3 AABB::GetMax() const
{
	glm::vec3 position		= GetPosition();
	glm::vec3 dimentions	= GetDimentions();

	return glm::vec3(position.x + (dimentions.x * 2.0f),
		position.y + (dimentions.y * 2.0f),
		position.z + (dimentions.z * 2.0f));
}
