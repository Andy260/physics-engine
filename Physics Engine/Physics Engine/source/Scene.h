#pragma once
#include <vector>
#include <glm\vec3.hpp>

namespace physics
{
	class RigidBody;
	class SpringJoint;

	class Scene
	{
	public:
		Scene(const glm::vec3& a_gravity);

		// Adds a new physics object to the scene
		bool AddActor(RigidBody* a_pActor);
		// Removes a physics object from the scene
		bool RemoveActor(RigidBody* a_pActor);

		bool AddJoint(SpringJoint* a_pJoint);

		bool RemoveJoint(SpringJoint* a_pJoint);
		
		// Runs a frame of simulation on this scene
		void Update(float a_deltaTime);

		// Returns all actors within this scene
		inline const std::vector<RigidBody*>& GetActors() const { return m_actors; }

		inline const std::vector<SpringJoint*>& GetSpringJoints() const { return m_springJoints; }

		inline glm::vec3 GetGravity() const { return m_gravity; }
		inline void SetGravity(const glm::vec3& a_gravity) { m_gravity = a_gravity; }

	private:
		std::vector<RigidBody*> m_actors;	// All objects within this scene
		std::vector<SpringJoint*> m_springJoints;

		glm::vec3 m_gravity;				// Gravity of the scene
	};
}
