#include "Engine.h"

// Dependancy includes
#include <Scene.h>
#include <Collision Handling\CollisionHandler.h>
#include <Utilities\Timer.h>

using namespace physics;

Engine::Engine() 
{
	m_pCollisionHandler = new CollisionHandler(m_scenes);
}

void Engine::Update(float a_deltaTime)
{
	// Check for collisions
	m_pCollisionHandler->CheckCollisions();

	// Update all scenes
	for (auto &i : m_scenes)
	{
		i->Update(a_deltaTime);
	}
}

bool Engine::AddScene(Scene* a_pScene)
{
	m_scenes.push_back(a_pScene);
	return true;
}

void Engine::Run()
{
	Timer deltaTimer(true);

	while (true)
	{
		float deltaTime = (float)deltaTimer.Elapsed().count();

		Update(deltaTime);
	}
}
