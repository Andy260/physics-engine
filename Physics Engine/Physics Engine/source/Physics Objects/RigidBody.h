#pragma once
#include <glm\vec3.hpp>
#include <glm\gtc\quaternion.hpp>

namespace physics
{
	class BoundingVolume;
	class CollisionHandler;

	class RigidBody
	{
	public:
		friend CollisionHandler;

		// Defines a rigid body type
		enum class Type
		{
			STATIC,		// No forces will be applied
			DYNAMIC,	// Forces affect this rigid body
		};

		RigidBody(float a_mass, Type a_type);

		RigidBody(const glm::vec3& a_position,
				  const glm::quat& a_rotation,
				  float a_mass, Type a_type);

		~RigidBody() {}

		void Update(const glm::vec3& a_gravity, float a_deltaTime);

		// Applies a force to this rigid body, adding to any current forces
		void ApplyForce(const glm::vec3& a_force);

		// Applies a torque to this rigid body, adding to any current forces
		void ApplyTorque(const glm::vec3& a_torque);

		inline Type GetType() const { return m_type; }
		inline void SetType(Type a_type) { m_type = a_type; }

		inline glm::vec3 GetPosition() const { return m_position; }
		inline void SetPosition(const glm::vec3& a_position) { m_position = a_position; }

		inline glm::vec3 GetVelocity() const { return m_velocity; }

		inline float GetMass() const { return m_mass; }
		inline void SetMass(float a_mass) 
		{
			// Ensure mass is never zero
			assert(a_mass > 0.0f);
			m_mass = a_mass; 
		}

		inline BoundingVolume* GetBoundingVolume() const 
		{ 
			return m_pBoundingVolume; 
		}
		void SetBoundingVolume(BoundingVolume* a_pBoundingVolume);

		inline float GetLinearDrag() const { return m_linearDrag; }
		void SetLinearDrag(float a_drag);

		inline float GetAngularDrag() const { return m_angularDrag; }
		void SetAngularDrag(float a_drag);

		inline float GetElasticity() const { return m_elasticity; }
		void SetElasticity(float a_elasticity);

	private:
		glm::vec3 m_position;				// Current position in the scene, in world space

		glm::quat m_rotation;				// Current rotation in the scene, in world space

		glm::vec3 m_velocity;				// Current velocity of this rigid body
											
		glm::vec3 m_angularVelo;			// Current angular velocity of this rigid body
											
		float m_mass;						// Mass of this rigid body, for collision response
		
		float m_linearDrag;					// Drag coefficient
		
		float m_angularDrag;				// Angular drag coefficient

		float m_elasticity;					// A value of 0-1, representing the percentage of energy lost when a collision occurs

		Type m_type;						// Type of rigid body this object is

		BoundingVolume* m_pBoundingVolume;	// Bounding volume to use for collision detection

		// Demonstrates Newton's thrid law to apply a force to both this rigid body,
		// and the given rigid body (used within collision response)
		void ApplyForceToActor(const glm::vec3& a_force, RigidBody* a_pActor);
	};
}
