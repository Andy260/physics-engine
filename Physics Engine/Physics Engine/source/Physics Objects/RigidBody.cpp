#include "RigidBody.h"
#include <Bounding Volumes\BoundingVolume.h>

using namespace physics;


RigidBody::RigidBody(float a_mass, Type a_type)
	: RigidBody(glm::vec3(0), glm::quat(glm::vec3()), a_mass, a_type)
{

}

RigidBody::RigidBody(const glm::vec3& a_position, const glm::quat& a_rotation,
					 float a_mass, Type a_type)
	: m_position(a_position), m_rotation(a_rotation), m_mass(a_mass),
	  m_type(a_type)
{

}

void RigidBody::Update(const glm::vec3& a_gravity, float a_deltaTime)
{
	// Don't update static rigid bodies
	if (m_type == Type::STATIC)
	{
		return;
	}

	// Apply gravity to rigid body
	m_velocity += a_gravity;

	// Update position, based upon current velocity and delta time
	m_position += m_velocity * a_deltaTime;
}

void RigidBody::ApplyForce(const glm::vec3& a_force)
{
	// Don't move static rigid bodies
	if (m_type == Type::STATIC)
	{
		return;
	}

	// Calculate acceleration
	glm::vec3 acceleration(a_force / m_mass);

	// Apply acceleration to current velocity
	m_velocity += acceleration;
}

void RigidBody::ApplyForceToActor(const glm::vec3& a_force, RigidBody* a_pActor)
{
	ApplyForce(-a_force);
	a_pActor->ApplyForce(a_force);
}

void RigidBody::ApplyTorque(const glm::vec3& a_torque)
{
	// TODO: Implement rotations
}

void RigidBody::SetBoundingVolume(BoundingVolume* a_pBoundingVolume)
{
	m_pBoundingVolume = a_pBoundingVolume;
	m_pBoundingVolume->SetRigidBody(this);
}

void RigidBody::SetLinearDrag(float a_drag)
{
	if (a_drag < 0.0f)
		m_linearDrag = 0.0f;
	else if (a_drag > 1.0f)
		m_linearDrag = 1.0f;
	else
		m_linearDrag = a_drag;
}

void RigidBody::SetAngularDrag(float a_drag)
{
	if (a_drag < 0.0f)
		m_angularDrag = 0.0f;
	else if (a_drag > 1.0f)
		m_angularDrag = 1.0f;
	else
		m_angularDrag = a_drag;
}

void RigidBody::SetElasticity(float a_elasticity)
{
	if (a_elasticity < 0.0f)
		m_elasticity = 0.0f;
	else if (a_elasticity > 1.0f)
		m_elasticity = 1.0f;
	else
		m_elasticity = a_elasticity;
}
