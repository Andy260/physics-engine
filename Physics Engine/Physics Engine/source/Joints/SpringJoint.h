#pragma once

namespace physics
{
	class RigidBody;

	class SpringJoint
	{
	public:
		SpringJoint(RigidBody* a_pConnection, RigidBody* a_pConnection1, 
			float a_springCoefficient, float a_damping, float a_restLength);
		~SpringJoint();

		void Update(float a_deltaTime);

		inline void SetDamping(float a_damping)
		{
			m_damping = a_damping;
		}

		inline void SetRestLength(float a_restLength)
		{
			m_restLength = a_restLength;
		}

		inline void SetSpringCoefficient(float a_springCoefficient)
		{
			m_springCoefficient = a_springCoefficient;
		}

		inline float GetDamping() const
		{
			return m_damping;
		}

		inline float GetRestLength() const
		{
			return m_restLength;
		}

		inline float GetSpringCoefficient() const
		{
			return m_springCoefficient;
		}

		inline RigidBody* GetFirstConnection() const
		{
			return m_pConnections[0];
		}

		inline RigidBody* GetSecondConnection() const
		{
			return m_pConnections[1];
		}

	private:
		RigidBody* m_pConnections[2];
		float m_damping;
		float m_restLength;
		float m_springCoefficient;
	};
}
