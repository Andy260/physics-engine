#include "SpringJoint.h"

// GLM includes
#include <glm\geometric.hpp>

// Project includes
#include <Physics Objects\RigidBody.h>

using namespace physics;

SpringJoint::SpringJoint(RigidBody* a_pConnection, RigidBody* a_pConnection1,
	float a_springCoefficient, float a_damping, float a_restLength)
	: m_damping(a_damping), m_springCoefficient(a_springCoefficient),
	  m_restLength(a_restLength)
{
	m_pConnections[0] = a_pConnection;
	m_pConnections[1] = a_pConnection1;

	glm::length(m_pConnections[0]->GetPosition() - 
		m_pConnections[1]->GetPosition());
}

SpringJoint::~SpringJoint()
{
}

void SpringJoint::Update(float a_deltaTime)
{
	glm::vec3 positionDelta = m_pConnections[1]->GetPosition() - 
		m_pConnections[0]->GetPosition();

	float curDistance = glm::length(positionDelta) - m_restLength;

	glm::vec3 direction = glm::normalize(positionDelta);

	float force = (curDistance * m_springCoefficient);

	glm::vec3 relativeVelo = m_pConnections[0]->GetVelocity() - 
		-m_pConnections[1]->GetVelocity();

	m_pConnections[0]->ApplyForce((direction * force) - 
		(relativeVelo * m_damping));
	m_pConnections[1]->ApplyForce((-direction * force) - 
		(relativeVelo * m_damping));
}
