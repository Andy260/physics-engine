#include "Scene.h"
#include <Bounding Volumes\BoundingVolume.h>
#include <Physics Objects\RigidBody.h>
#include <Joints\SpringJoint.h>

using namespace physics;


Scene::Scene(const glm::vec3& a_gravity)
	: m_gravity(a_gravity)
{

}

void Scene::Update(float a_deltaTime)
{
	// Update all joints
	for (auto &i : m_springJoints)
	{
		i->Update(a_deltaTime);
	}

	// Update all actors within this scene
	// (collisions are handled by Engine)
	for (auto &i : m_actors)
	{
		i->Update(GetGravity(), a_deltaTime);
	}
}

bool Scene::AddActor(RigidBody* a_pActor)
{
	if (std::find(m_actors.begin(), m_actors.end(), a_pActor) == m_actors.end())
	{
		// Actor not in this scene, so add it
		m_actors.push_back(a_pActor);
		return true;
	}

	// Actor either is already in scene, or null pointer given
	return false;
}

bool Scene::RemoveActor(RigidBody* a_pActor)
{
	// Find Actor within scene
	auto actorItr = std::find(m_actors.begin(), m_actors.end(), a_pActor);

	if (actorItr == m_actors.end())
	{
		// Actor can't be found
		return false;
	}

	// Remove Actor from scene
	m_actors.erase(actorItr);
	return true;
}

bool Scene::AddJoint(SpringJoint* a_pJoint)
{
	if (std::find(m_springJoints.begin(), 
			m_springJoints.end(), a_pJoint) == m_springJoints.end())
	{
		// Actor not in this scene, so add it
		m_springJoints.push_back(a_pJoint);
		return true;
	}

	// Actor either is already in scene, or null pointer given
	return false;
}

bool Scene::RemoveJoint(SpringJoint* a_pJoint)
{
	// Find Actor within scene
	auto actorItr = std::find(m_springJoints.begin(), m_springJoints.end(), 
		a_pJoint);

	if (actorItr == m_springJoints.end())
	{
		// Actor can't be found
		return false;
	}

	// Remove Actor from scene
	m_springJoints.erase(actorItr);
	return true;
}
