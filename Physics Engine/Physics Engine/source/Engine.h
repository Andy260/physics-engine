#pragma once

// STL includes
#include <vector>

namespace physics
{
	class Scene;
	class RigidBody;
	class BoundingVolume;
	class Sphere;
	class Plane;
	class CollisionHandler;

	class Engine
	{
	public:
		Engine();
		~Engine() {}

		// Runs a single tick of simulation given the current delta time
		// may not actually update each scene, depending on whether or not
		// their timestep is greater or current than the given delta time
		void Update(float a_deltaTime);

		// Runs the simulation on all scenes, per time-step for each scene,
		// useful for running the physics engine on a serpate thread
		void Run();

		// Adds a scene to the simulation, will return false if this failed
		bool AddScene(Scene* a_pScene);

		inline float GetTimeStep() const { return m_timeStep; }
		inline void SetTimeStep(float a_timeStep) { m_timeStep = a_timeStep; }

	private:
		float m_timeStep;						// How often should the simulation update each scene

		std::vector<Scene*> m_scenes;			// List of all scenes to be simulated

		CollisionHandler* m_pCollisionHandler;	// Object used to detect and respond to collisions within every scene
	};
}
