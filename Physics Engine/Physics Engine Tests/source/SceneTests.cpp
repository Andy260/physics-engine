#include "stdafx.h"
#include <CppUnitTest.h>

// Physics Engine includes
#include <Scene.h>
#include <Physics Objects\RigidBody.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

typedef physics::RigidBody::Type rigidBodyType;

namespace PhysicsEngineTests
{
	TEST_CLASS(SceneTests)
	{
	public:
		TEST_METHOD(Add_Actor)
		{
			physics::Scene scene(glm::vec3(0));
			physics::RigidBody actor(1.0f, rigidBodyType::DYNAMIC);

			scene.AddActor(&actor);

			const std::vector<physics::RigidBody*>& actors = scene.GetActors();
			Assert::IsTrue(actors.size() == 1);
		}

		TEST_METHOD(Remove_Actor)
		{

		}
	};
}