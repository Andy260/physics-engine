#include "stdafx.h"
#include <CppUnitTest.h>

#include <vector>

// Physics Engine includes
#include <Physics Objects\RigidBody.h>
#include <Bounding Volumes\Sphere.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

typedef physics::RigidBody::Type rigidBodyType;

namespace PhysicsEngineTests
{
	TEST_CLASS(RigidBody)
	{
	public:
		TEST_METHOD(Add_Bounding_Volume)
		{
			physics::RigidBody rigidBody(1.0f, rigidBodyType::DYNAMIC);
			physics::Sphere sphere(glm::vec3(0), 1.0f);

			rigidBody.SetBoundingVolume(&sphere);

			Assert::IsNotNull(rigidBody.GetBoundingVolume());
		}

		TEST_METHOD(Remove_Bounding_Volume)
		{

		}
	};
}