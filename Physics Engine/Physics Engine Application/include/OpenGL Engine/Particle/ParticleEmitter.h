#pragma once
#include <vector>
#include <glm\glm.hpp>
#include <Render\Shader.h>

class Texture2D;
class Camera;
class DirectionalLight;

class ParticleEmitter
{
protected:
	struct Particle
	{
		glm::vec3 position;
		glm::vec3 velocity;
		glm::vec4 colour;
		float size;
		float lifetime;
		float lifespan;
	};

	// Defines a vertex used by particle objects
	struct Vertex
	{
		glm::vec4 position;
		glm::vec4 colour;
		glm::vec2 texCoord;
	};

	Shader m_shader;

	Texture2D* m_pTexture;

	Particle* m_particles;
	Vertex* m_vertexData;

	// Interator to first dead particle in list
	unsigned int m_firstDead;

	// OpenGL object handles
	unsigned int m_vao, m_vbo, m_ibo;

	unsigned int m_maxParticles;

	// Position of this emitter
	glm::vec3 m_position;

	float m_emitTimer; 
	float m_emitRate;

	float m_lifespanMin; 
	float m_lifespanMax;

	float m_velocityMin;
	float m_velocityMax;

	float m_startSize;
	float m_endSize;

	glm::vec4 m_startColour;
	glm::vec4 m_endColour;

	void Emit();

public:
	ParticleEmitter();
	virtual ~ParticleEmitter();

	void Initialise(unsigned int a_maxParticles,
					unsigned int a_emitRate,
					float a_lifetimeMin, float a_lifetimeMax,
					float a_velocityMin, float a_velocityMax,
					float a_startSize, float a_endSize,
					const glm::vec4& a_startColour, 
					const glm::vec4& a_endColour,
					std::string a_texturePath);

	virtual void Update(const float& a_deltaTime, const glm::mat4& a_cameraTransform);
	void Draw(const Camera& a_camera,
			  const DirectionalLight& a_dirLight);

	inline glm::vec3 GetPosition() const { return m_position; }
	inline void SetPosition(const glm::vec3& a_position) { m_position = a_position; }
};