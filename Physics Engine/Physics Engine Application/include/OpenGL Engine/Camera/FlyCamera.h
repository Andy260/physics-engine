#pragma once
#include <Camera\Camera.h>
#include <glm\glm.hpp>

class GameWindow;

class FlyCamera : public Camera
{
private:
	GameWindow* m_pWindow;

	glm::vec2 m_oldMousePos;

	float m_speed;
	bool m_rotating;

	void HandleMouseInput();
	void HandleKeyboardInput(const float& a_deltaTime);

public:
	FlyCamera(GameWindow* a_pWindow);
	FlyCamera(const glm::mat4 &a_transform, const float &a_speed);

	void Update(const float &a_deltaTime);
	void Translate(const glm::vec3 &a_offset);
	void Rotate(const glm::vec2 &a_pitchYaw);

	inline float GetSpeed() const { return m_speed; }
	inline void SetSpeed(const float &a_speed) { m_speed = a_speed; }
};