#pragma once
#include <string>

typedef struct CTwBar TwBar;

class TweakMenu
{
private:
	// Should this tweak menu be drawn and capture input?
	bool m_active;

	// Handle to AntTweakBar
	TwBar* m_pTwBar;

	// Name of this tweak menu
	std::string m_name;

public:
	TweakMenu(std::string a_name);

	static void Intialise();
	static void Deintialise();

	static void DrawAllMenus();

	void AddVariable();

	inline void SetActive(const bool& a_value) { m_active = a_value; }

	inline std::string GetName() const { return m_name; }
	inline bool IsActive() const { return m_active; }
};