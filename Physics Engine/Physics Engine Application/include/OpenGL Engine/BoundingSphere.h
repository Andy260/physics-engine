#pragma once
#include <glm\vec3.hpp>
#include <vector>
#include <list>

class BoundingBox;
class BoundingFrustum;
class Plane;

class BoundingSphere
{
private:
	glm::vec3 m_centre;
	float m_radius;

public:
	BoundingSphere(const glm::vec3& a_centre, const float& a_radius);

	void Fit(const std::vector<glm::vec3>& a_pointsList);
	void Fit(const std::vector<glm::vec3*>& a_pointsList);
	void Fit(const std::list<glm::vec3>& a_pointsList);
	void Fit(const std::list<glm::vec3*>& a_pointsList);

	bool Intersects(const BoundingBox& a_box);
	bool Intersects(const BoundingSphere& a_sphere);
	bool Intersects(const BoundingFrustum& a_frustum);
	bool Intersects(const Plane& a_plane);

	inline void SetCentre(const glm::vec3& a_centre) { m_centre = a_centre; }
	inline void SetRadius(const float& a_radius) { m_radius = a_radius; }

	inline glm::vec3 GetCentre() const { return m_centre; }
	inline float GetRadius() const { return m_radius; }
};