#pragma once
#include <list>
#include <glm\mat4x4.hpp>

class Node
{
protected:
	Node* m_pParent;
	std::list<Node*> m_children;

	glm::mat4 m_worldTransform;
	glm::mat4 m_localTransform;

	void UpdateTransforms();

public:
	Node();


	void SetParent(Node* a_node);
	void AddChild(Node* a_node);
	void RemoveChild(Node* a_node);

	void SetLocalTransform(const glm::mat4& a_transform);

	inline Node* GetParent() const { return m_pParent; }
	inline std::list<Node*> GetChildren() const { return m_children; }

	inline glm::mat4 GetLocalTransform() const { return m_localTransform; }
	inline glm::mat4 GetWorldTransform() const { return m_worldTransform; }
};