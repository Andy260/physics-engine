#pragma once
#include <string>
#include <FBXFile.h>
#include <glm\mat4x4.hpp>

class Shader;
class Camera;
class DirectionalLight;

class FBXModel
{
private:
	static Shader* m_pStatic;		// static mesh with no texture
	static Shader* m_pStaticTex;	// static mesh with diffuse texture
	static int m_modelCount;

	FBXFile m_file;

	glm::mat4 m_worldTransform;

	void LoadShaders();

public:
	FBXModel();
	~FBXModel();

	void Load(std::string a_filePath);

	void Draw(const Camera& a_camera, const DirectionalLight& a_dirLight);

	glm::mat4 GetTransform()								const;
	glm::mat4 GetNodeTransform(const int& a_nodeID,
							   glm::mat4& a_localTransform,
							   glm::mat4& a_worldTransform) const;

	void SetTransform(const glm::mat4& a_transform);
	void SetNodeTransform(const int& a_nodeID,
						  const glm::mat4& a_localTransform,
						  const glm::mat4& a_worldTransform);
};