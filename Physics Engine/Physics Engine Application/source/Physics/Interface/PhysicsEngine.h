#pragma once
#include <vector>

namespace physicsApp
{
	class PhysicsScene;
	class PhysicsActor;

	// Base class for the physics engine interface
	// for interfacing between Physx and my physics engine
	class PhysicsEngine
	{
	public:
		PhysicsEngine() {}
		virtual ~PhysicsEngine() {}

		virtual PhysicsScene* CreateScene()					= 0;
		virtual bool RemoveScene(PhysicsScene* a_pScene)	= 0;

		virtual void Run()									= 0;

	private:
		virtual void Initialise()							= 0;
		virtual void Update(float a_deltaTime)				= 0;
	};
}
