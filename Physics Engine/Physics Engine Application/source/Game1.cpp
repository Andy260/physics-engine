#include "Game1.h"

// STL includes
#include <cfloat>

// Dependency includes
#include <GameWindow.h>
#include <Gizmos.h>
#include <gl_core_4_4.h>
#include <glm\gtc\constants.hpp>
#include <Input\Keyboard.h>

// Physics Engine includes
#include <Engine.h>
#include <Scene.h>
#include <Bounding Volumes\Plane.h>
#include <Bounding Volumes\Sphere.h>
#include <Bounding Volumes\AABB.h>
#include <Physics Objects\RigidBody.h>
#include <Joints\SpringJoint.h>

using namespace physicsApp;

typedef physics::RigidBody::Type rigidBodyType;

Game1::Game1() 
	: Game(1280, 720, "Physics Engine"),
	  m_camera(m_pWindow),
	  m_physicsVisualiser(m_boundingVolumes, m_springJoints)
{

}

void Game1::SetupSpring()
{
	const int connectionsCount = 4;

	physics::Sphere* rigidBodies[connectionsCount];
	physics::SpringJoint* joints[connectionsCount - 1];

	// Create Rigid bodies
	for (unsigned int i = 0; i < connectionsCount; ++i)
	{
		rigidBodyType type = rigidBodyType::DYNAMIC;
		if (i == 0)
		{
			type = rigidBodyType::STATIC;
		}

		glm::vec3 spawnPos(3.0f + ((float)i * 3.0f),
			15.0f, 0.0f);

		rigidBodies[i] = CreateSphere(spawnPos, 10.0f, 0.5f, type);
	}

	// Create Joints
	for (unsigned int i = 0; i < connectionsCount - 1; ++i)
	{
		joints[i] = new physics::SpringJoint(rigidBodies[i]->GetRigidBody(), 
			rigidBodies[i + 1]->GetRigidBody(), 0.5f, 0.001f, 3.0f);
		
		m_springJoints.push_back(joints[i]);
		m_pScene->AddJoint(joints[i]);
	}
}

void Game1::SetupPhysics()
{
	m_pPhysicsEngine = new physics::Engine();

	// Create physics scene with downwards gravity, and 0.1 second timestep
	m_pScene = new physics::Scene(glm::vec3(0, -0.01f, 0));
	m_pPhysicsEngine->AddScene(m_pScene);

	// Create actors
	float sphereSize	= 1.0f;
	float actorMass		= 10.0f;

	CreatePlane(glm::vec3(0, 1, 0), -0.0f);

	m_pSphere = CreateSphere(glm::vec3(-1.5f, 3, 0), actorMass, 
		sphereSize, rigidBodyType::DYNAMIC);
	m_pSphere1 = CreateSphere(glm::vec3(1.5f, 6, 0), actorMass, 
		sphereSize, rigidBodyType::DYNAMIC);
	m_pAABB = CreateAABB(glm::vec3(0, 9, 1.5f), 10.0f, glm::vec3(1, 1, 1), 
		rigidBodyType::DYNAMIC);
	m_pAABB1 = CreateAABB(glm::vec3(0, 9, -1.5f), 10.0f, glm::vec3(1, 1, 1),
		rigidBodyType::DYNAMIC);

	SetupSpring();
}

physics::Sphere* Game1::CreateSphere(const glm::vec3& a_pos, float a_mass, 
	float a_radius, rigidBodyType a_type)
{
	// Create RigidBody
	physics::RigidBody* pActor = new physics::RigidBody(a_pos, glm::quat(), 
		a_mass, a_type);

	// Create sphere and add it to RigidBody
	physics::Sphere* pSphere = new physics::Sphere(glm::vec3(), a_radius);
	pActor->SetBoundingVolume(pSphere);

	// Add actor to scene
	bool status = m_pScene->AddActor(pActor);
	assert(status);

	// Store pointer in list
	m_actors.push_back(pActor);
	m_boundingVolumes.push_back(pSphere);

	return pSphere;
}

physics::AABB* Game1::CreateAABB(const glm::vec3& a_pos, float a_mass, 
	const glm::vec3& a_dims, rigidBodyType a_type)
{
	// Create RigidBody
	physics::RigidBody* pActor = new physics::RigidBody(a_pos, glm::quat(),
		a_mass, a_type);

	// Create AABB and add it to Rigidbody
	physics::AABB* pAABB = new physics::AABB(glm::vec3(), a_dims);
	pActor->SetBoundingVolume(pAABB);

	// Add actor to scene
	bool status = m_pScene->AddActor(pActor);
	assert(status);

	// Store pointer in list
	m_actors.push_back(pActor);
	m_boundingVolumes.push_back(pAABB);

	return pAABB;
}

physics::Plane* Game1::CreatePlane(const glm::vec3& a_normal, 
	float a_distToNormal)
{
	// Create RigidBody
	physics::RigidBody* pActor = new physics::RigidBody(glm::vec3(), glm::quat(),
		0.0f, rigidBodyType::DYNAMIC);

	// Create plane and add it to Rigidbody
	physics::Plane* pPlane = new physics::Plane(a_normal, a_distToNormal);
	pActor->SetBoundingVolume(pPlane);

	// Add actor to scene
	bool status = m_pScene->AddActor(pActor);
	assert(status);

	// Store pointer in list
	m_actors.push_back(pActor);
	m_boundingVolumes.push_back(pPlane);

	return pPlane;
}

void Game1::Initialise()
{
	// Setup Rendering
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Get window size
	int winWidth, winHeight;
	m_pWindow->GetSize(&winWidth, &winHeight);

	// Setup camera
	m_camera.LookAt(glm::vec3(22, 22, -33), glm::vec3(0, 0, 0),
		glm::vec3(0, 1, 0));
	m_camera.SetProjection(glm::pi<float>() * 0.25f,
		(float)winWidth / (float)winHeight, 0.1f, 1000.0f);
	m_camera.SetSpeed(10.0f);

	Gizmos::create();

	SetupPhysics();
}

void Game1::Update(const float& a_deltaTime)
{
	m_camera.Update(a_deltaTime);

	physics::RigidBody* pSphere = m_pSphere->GetRigidBody();
	physics::RigidBody* pSphere1 = m_pSphere1->GetRigidBody();

	physics::RigidBody* pAABB = m_pAABB->GetRigidBody();
	physics::RigidBody* pAABB1 = m_pAABB1->GetRigidBody();

	if (Keyboard::IsKeyDown(Keys::KEY_UP))
	{
		pSphere->ApplyForce(glm::vec3(0.0f, 0.01f, 0.0f));
		pSphere1->ApplyForce(glm::vec3(0.0f, 0.01f, 0.0f));

		pAABB->ApplyForce(glm::vec3(0.0f, -0.01f, 0.0f));
		pAABB1->ApplyForce(glm::vec3(0.0f, -0.01f, 0.0f));
	}
	else if (Keyboard::IsKeyDown(Keys::KEY_DOWN))
	{
		pSphere->ApplyForce(glm::vec3(0.0f, -0.01f, 0.0f));
		pSphere1->ApplyForce(glm::vec3(0.0f, -0.01f, 0.0f));

		pAABB->ApplyForce(glm::vec3(0.0f, 0.01f, 0.0f));
		pAABB1->ApplyForce(glm::vec3(0.0f, 0.01f, 0.0f));
	}

	m_pPhysicsEngine->Update(a_deltaTime);
}

void Game1::Draw(const float& a_deltaTime)
{
	// Clear rendering
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	Gizmos::clear();

	// Render actors
	m_physicsVisualiser.Draw();

	// Render grid
	Gizmos::addTransform(glm::mat4(1));
	glm::vec4 white(1);
	glm::vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(glm::vec3(-10 + i, 0, 10),
			glm::vec3(-10 + i, 0, -10),
			i == 10 ? white : black);
		Gizmos::addLine(glm::vec3(10, 0, -10 + i),
			glm::vec3(-10, 0, -10 + i),
			i == 10 ? white : black);
	}

	// Render rigid body Gizmos
	Gizmos::draw(m_camera.GetProjectionViewTransform());
}

void Game1::Shutdown()
{
	Gizmos::destroy();
}
