#pragma once

// STL includes
#include <list>

// Dependancy includes
#include <glm\vec3.hpp>
#include <glm\vec4.hpp>

namespace physics { class BoundingVolume; class Sphere; class AABB; 
	class SpringJoint; class RigidBody; }

namespace physicsApp
{
	class PhysicsVisualiser
	{
	public:
		PhysicsVisualiser(
			const std::list<physics::BoundingVolume*>& a_boundingVolumes,
			const std::list<physics::SpringJoint*>& a_springJoints);
		~PhysicsVisualiser() {}

		void Draw();

	private:
		const std::list<physics::BoundingVolume*>& m_boundingVolumes;	// List of all bounding volumes to render
		const std::list<physics::SpringJoint*>& m_springJoints;

		void RenderActor(physics::Sphere* a_actor, const glm::vec4& a_colour);
		void RenderActor(physics::AABB* a_actor, const glm::vec4& a_colour);
		void RenderJoint(physics::RigidBody* a_pActor, physics::RigidBody* a_pActor1);
	};
}
