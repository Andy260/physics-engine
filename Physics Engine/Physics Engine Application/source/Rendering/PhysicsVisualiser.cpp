#include "PhysicsVisualiser.h"

// Dependancy includes
#include <Bounding Volumes\AABB.h>
#include <Bounding Volumes\Sphere.h>
#include <Physics Objects\RigidBody.h>
#include <Joints\SpringJoint.h>
#include <Gizmos.h>

using namespace physicsApp;

PhysicsVisualiser::PhysicsVisualiser(
	const std::list<physics::BoundingVolume*>& a_boundingVolumes,
	const std::list<physics::SpringJoint*>& a_springJoints)
	: m_boundingVolumes(a_boundingVolumes),
	  m_springJoints(a_springJoints)
{

}

void PhysicsVisualiser::RenderActor(physics::Sphere* a_actor, 
	const glm::vec4& a_colour)
{
	// Prevent null pointer dereferences
	if (a_actor == nullptr)
		return;

	// Render actor
	Gizmos::addSphere(a_actor->GetPosition(),
		a_actor->GetRadius(), 15, 15, a_colour);
}

void PhysicsVisualiser::RenderActor(physics::AABB* a_actor,
	const glm::vec4& a_colour)
{
	// Prevent null pointer dereferences
	if (a_actor == nullptr)
		return;

	// Render actor
	Gizmos::addAABBFilled(a_actor->GetPosition(), a_actor->GetDimentions(), 
		a_colour);
}

void PhysicsVisualiser::RenderJoint(physics::RigidBody* a_pActor,
	physics::RigidBody* a_pActor1)
{
	Gizmos::addLine(a_pActor->GetPosition(), a_pActor1->GetPosition(), glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
}

void PhysicsVisualiser::Draw()
{
	for (auto &i : m_boundingVolumes)
	{
		// Render Sphere, if current object is one
		physics::Sphere* pSphere = dynamic_cast<physics::Sphere*>(i);
		if (pSphere != nullptr)
		{
			RenderActor(pSphere, glm::vec4(1, 0, 0, 1));
			continue;
		}

		// Render AABB, if current object is one
		physics::AABB* pAABB = dynamic_cast<physics::AABB*>(i);
		if (pAABB != nullptr)
		{
			RenderActor(pAABB, glm::vec4(0, 0, 1, 1));
			continue;
		}
	}

	// Render all joints
	for (auto &joint : m_springJoints)
	{
		RenderJoint(joint->GetFirstConnection(), joint->GetSecondConnection());
	}
}
