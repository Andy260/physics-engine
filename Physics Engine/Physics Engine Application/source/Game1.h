#pragma once
#include <Game.h>

// Dependancy includes
#include <Camera\FlyCamera.h>
#include <Rendering\PhysicsVisualiser.h>
#include <Physics Objects\RigidBody.h>

// STL includes
#include <list>

namespace physics { class Engine; class Scene; class Sphere; 
	class Plane; class AABB; class SpringJoint; }

namespace physicsApp
{
	class Game1 : public Game
	{
	public:
		Game1();
		~Game1() {}

	private:
		PhysicsVisualiser m_physicsVisualiser;						// Object used to render all physics actors

		FlyCamera m_camera;											// Main scene camera

		physics::Engine* m_pPhysicsEngine;							// Physics Engine library

		physics::Sphere* m_pSphere;
		physics::Sphere* m_pSphere1;
		physics::AABB* m_pAABB;
		physics::AABB* m_pAABB1;

		std::list<physics::RigidBody*> m_actors;					// List of all actors
		std::list<physics::BoundingVolume*> m_boundingVolumes;		// List of all bounding volumes
		std::list<physics::SpringJoint*> m_springJoints;			// List of all spring joints

		physics::Scene* m_pScene;									// Physics scene containing all physics objects

		void SetupPhysics();

		physics::Sphere* CreateSphere(const glm::vec3& a_pos, float a_mass, 
									  float a_radius,
									  physics::RigidBody::Type a_type);

		physics::AABB* CreateAABB(const glm::vec3& a_pos, float a_mass,
								  const glm::vec3& a_dims, 
								  physics::RigidBody::Type a_type);

		physics::Plane* CreatePlane(const glm::vec3& a_normal, 
									float a_distToNormal);

		void SetupSpring();

		void Initialise()						override final;
		void Update(const float &a_deltaTime)	override final;
		void Draw(const float &a_deltaTime)		override final;
		void Shutdown()							override final;
	};
}
