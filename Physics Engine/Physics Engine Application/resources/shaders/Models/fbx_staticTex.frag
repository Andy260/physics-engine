#version 410

struct Material
{
	vec4 ambient;			// Ambient colour (intensity stored in a)
	vec4 diffuse;			// Diffuse colour
	vec4 specular; 			// Specular colour (shininess stored in a)
	sampler2D diffuseTex;	// Diffuse texture
};

struct DirectionalLight
{
	vec3 direction;				// Direction of this light
	vec3 ambient;				// Ambient colour of this light
	vec3 diffuse;				// Colour of this light
	float ambientIntensity;		// Intensity of ambient lighting
	float diffuseIntensity;		// Intensity of diffuse light
	float specularIntensity;	// Intensity of specular highlights
};

// Vertex inputs
in vec3 fNormal;
in vec3 fPosition;
in vec3 fTangent;
in vec3 fBitangent;	
in vec2 fTexCoord;

// Final fragment colour
out vec4 fragColour;

uniform DirectionalLight dirLight;	// Directional light properties
uniform Material material;			// Mesh's lighting properties
uniform vec3 cameraPos;				// Used for specular highlighting

void main()
{
	vec3 ambient = material.ambient.rgb * dirLight.ambient;
	
	float NdL = max(0.0f, dot(normalize(fNormal), dirLight.direction));
	vec3 diffuse = material.diffuse.rgb *  dirLight.diffuse * NdL;
		
	vec3 R = reflect(dirLight.direction, cameraPos);
	vec3 E = normalize(cameraPos - fPosition);
	
	float specTerm = pow(min(0.0f, dot(R, E)), dirLight.specularIntensity);
	vec3 specular = material.specular.rgb * dirLight.diffuse * dirLight.specularIntensity;
	
	fragColour = texture(material.diffuseTex, fTexCoord) * vec4(ambient + diffuse + specular, 1);
}